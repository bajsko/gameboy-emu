//
//  mesh.c
//  gameboyemu
//
//  Code from my old multisnake project.
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include "mesh.h"
#include <stdlib.h>
#include <string.h>

mesh_t* mesh_make(shader_t* shaderToUse, int size)
{
    mesh_t* mesh = (mesh_t*)malloc(sizeof(mesh_t));
    if(!mesh)
        return NULL;
    
    mesh->data = malloc(sizeof(float) * size);
    if(!mesh->data)
    {
        free(mesh);
        return NULL;
    }
    
    mesh->shader = shaderToUse;
    
    GLuint vao = 0, vbo = 0;
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    
    mesh->vao = vao;
    mesh->vbo = vbo;
    mesh->vertices = -1;
    
    return mesh;
}

mesh_t* mesh_make_quad(int width, int height, shader_t* shader)
{
    float x = width/2;
    float y = height/2;
    
    float vertices[] = {
        -x, -y, 0, 0,
        x, -y, 1.0, 0,
        x, y, 1.0, 1.0,
        -x, y, 0.0, 1.0,
        x, y, 1.0, 1.0,
        -x, -y, 0, 0
    };
    
    mesh_t* mesh = mesh_make(shader, sizeof(vertices));
    
    if(!mesh)
        return NULL;
    
    mesh_upload(vertices, sizeof(vertices), GL_STATIC_DRAW, mesh);
    return mesh;

}

int mesh_upload(float* data, int size, int drawMode, mesh_t* mesh)
{
    
    if(!mesh || !mesh->shader || !mesh->data)
    {
        return -1;
    }
    
    memcpy(mesh->data, data, sizeof(float) * size);
    
    glBindVertexArray(mesh->vao);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
    glBufferData(GL_ARRAY_BUFFER, size, mesh->data, drawMode);
    
    shader_set_attribute("a_position", 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0, mesh->shader);
    shader_set_attribute("a_textureCoord", 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (2*sizeof(float)), mesh->shader);
    mesh->vertices = size/4;
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    return 0;
}
void mesh_set_active(mesh_t* mesh)
{
    glBindVertexArray(mesh->vao);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
}
int mesh_render(mesh_t* mesh)
{
    shader_bind(mesh->shader);
    mesh_set_active(mesh);
    glDrawArrays(GL_TRIANGLES, 0, mesh->vertices);
    shader_unbind();
    return 0;
}
int mesh_delete(mesh_t* mesh)
{
    glDeleteBuffers(1, &mesh->vbo);
    glDeleteVertexArrays(1, &mesh->vao);
    free(mesh->data);
    free(mesh);
    return 0;
}