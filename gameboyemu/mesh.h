//
//  mesh.h
//  gameboyemu
//
//  Code from my old multisnake project.
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef mesh_h
#define mesh_h

#include <stdio.h>
#include <OpenGL/gl3.h>
#include <GLFW/glfw3.h>

#include "shader.h"

typedef struct mesh
{
    GLuint vao;
    GLuint vbo;
    shader_t* shader;
    float* data;
    int vertices;
} mesh_t;

mesh_t* mesh_make(shader_t* shaderToUse, int size);
mesh_t* mesh_make_quad(int width, int height, shader_t* shader);
int mesh_upload(float* data, int size, int drawMode, mesh_t* mesh);
void mesh_set_active(mesh_t* mesh);
int mesh_render(mesh_t* mesh);
int mesh_delete(mesh_t* mesh);

#endif /* mesh_h */
