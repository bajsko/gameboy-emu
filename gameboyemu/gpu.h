//
//  gpu.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef gpu_h
#define gpu_h

#include <stdio.h>

#include "cpu.h"
#include "gbutil.h"

//LCD Control bits
static const char kGBLCDEnableBit = 7;
static const char kGBLCDCWindowTileMapDisplayBit = 6;
static const char kGBLCDCWindwDisplayEnableBit = 5;
static const char kGBLCDCBGWindwTDBit = 4;
static const char kGBLCDCBGTileMapDisplayBit = 3;
static const char kGBLCDCOBJSizeBit = 2;
static const char kGBLCDCOBJEnableBit = 1;
static const char kGBLCDCBGDisplayEnableBit = 0;

static const char kGBVBlank = 1;
static const char kGBHBlank = 0;
static const char kGBSearchOAM = 2;
static const char kGBTransfer = 3;
static const char kGBModeReset = 2;

static const char kGBMode0InterruptBit = 3;
static const char kGBMode1InterruptBit = 4;
static const char kGBMode2InterruptBit = 5;
static const char kGBCoincidenceBit = 6;

static const char kGBSizeOfTile = 16;

static const ushort kGBBGWindowPalleteDataLocation = 0xff47;
static const ushort kGBOBJPaletteDataLocation0 = 0xff48;
static const ushort kGBOBJPaletteDataLocation1 = 0xff49;

typedef enum GB_COLOR
{
    GB_WHITE,
    GB_LIGHT_GRAY,
    GB_DARK_GRAY,
    GB_BLACK
} GB_COLOR;

typedef struct gpu
{
    uchar lcdc;
    uchar stat;
    uchar scroll_y;
    uchar scroll_x;
    uchar scanline;
    uchar scanlineCompare;
    uchar window_x;
    uchar window_y;
    int ticks;
} gpu_t;

void gb_gpu_attach(cpu_t* cpu);
void gb_gpu_set_frame_buffer(uchar frameBuffer[][GB_RES_X][3]);
void gb_gpu_detach();

uchar gb_gpu_is_display_enabled();

gpu_t* gb_gpu_get_data();

void gb_gpu_step();

#endif /* gpu_h */
