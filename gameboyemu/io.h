//
//  io.h
//  gameboyemu
//
//  Allows reading and writing to files.
//
//  Created by Klas Henriksson on 2016-09-08.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef io_h
#define io_h

#include <stdio.h>
#include <stdlib.h>

int readFileToBuffer(unsigned char* dst, const char* string, unsigned long* fileSize);
int gb_getIOError();

#endif /* io_h */
