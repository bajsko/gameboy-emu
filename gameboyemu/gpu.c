//
//  gpu.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

//http://www.codeslinger.co.uk/pages/projects/gameboy/lcd.html
//http://www.codeslinger.co.uk/pages/projects/gameboy/graphics.html
//http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-Graphics

#include <assert.h>

#include "gpu.h"
#include "memory.h"
#include "interrupts.h"

static cpu_t* cpu = NULL;
static gpu_t gpu = { 0 };
static uchar (*frameBuffer)[GB_RES_X][3];

void gb_gpu_attach(cpu_t* lcpu)
{
    assert(lcpu != NULL && cpu == NULL);
    cpu = lcpu;
    
    gb_log("GPU: Attached to CPU!");
}

void gb_gpu_set_frame_buffer(uchar lframeBuffer[][GB_RES_X][3])
{
    assert(lframeBuffer != NULL);
    frameBuffer = lframeBuffer;
}

void gb_gpu_detach()
{
    assert(cpu != NULL);
    cpu = NULL;
    
    gb_log("GPU: Detached from CPU!");
    
}

uchar gb_gpu_is_display_enabled()
{
    //assert(cpu != NULL);
    return fetchBit(&gpu.lcdc, kGBLCDEnableBit);
}

gpu_t* gb_gpu_get_data()
{
    //assert(cpu != NULL);
    return &gpu;
}

static void UpdateLCDStatus()
{
    uchar status = gpu.stat;
    
    if(gb_gpu_is_display_enabled() == 0)
    {
        gpu.ticks = 456;
        gpu.scanline = 0;
        status &= 252;
        setBit(&status, 0, 1);
        gpu.stat = status;
        return;
    }
    
    uchar currentMode = status & kGBMode0InterruptBit;
    uchar mode = 0;
    uchar interruptBit = 0;
    
    if(gpu.scanline >= 144) //in V-BLANK
    {
        mode = 1;
        setBit(&status, 0, 1);
        setBit(&status, 1, 0);
        interruptBit = status & kGBMode1InterruptBit;
    } else {
        
        ushort mode2 = 456 - 80;
        ushort mode3 = mode2 - 172;
        
        if(gpu.ticks >= mode2) //mode 2
        {
            mode = 2;
            setBit(&status, 1, 1);
            setBit(&status, 0, 0);
            interruptBit = status & kGBMode2InterruptBit;
        } else if(gpu.ticks >= mode3) //mode 3
        {
            mode = 3;
            setBit(&status, 1, 1);
            setBit(&status, 0, 1);
        } else //mode 0
        {
            //hblank
            mode = 0;
            setBit(&status, 1, 0);
            setBit(&status, 0, 0);
            interruptBit = status & kGBMode0InterruptBit;
        }
    }
    
    if(interruptBit != 0 && (mode != currentMode))
    {
        gb_interrupt_request(kGBIFLCDStat);
    }
    
    if(gpu.scanline == gpu.scanlineCompare)
    {
        setBit(&status, 2, 1);
        if(fetchBit(&status, kGBCoincidenceBit) == 1)
        {
            gb_interrupt_request(kGBIFLCDStat);
        }
    } else {
        setBit(&status, 2, 0);
    }
    
    gpu.stat = status;
}

static GB_COLOR GetColor(uchar colorNum, ushort adress)
{
    GB_COLOR color = GB_BLACK;
    
    uchar pallette = readByte(adress);
    uchar hi = 0, lo = 0;
    
    switch (colorNum) {
        case 00:
            hi = 1;
            lo = 0;
            break;
            
        case 1:
            hi = 3;
            lo = 2;
            break;
            
        case 2:
            hi = 5;
            lo = 4;
            break;
            
        case 3:
            hi = 7;
            lo = 6;
            break;
            
        default:
            break;
    }
    
    int gbColor = 0;
    gbColor |= fetchBit(&pallette, hi) << 1;
    gbColor |= fetchBit(&pallette, lo);
    
    switch (gbColor) {
        case 0:
            color = GB_WHITE;
            break;
            
        case 1:
            color = GB_LIGHT_GRAY;
            break;
            
        case 2:
            color = GB_DARK_GRAY;
            break;
            
        case 3:
            color = GB_BLACK;
            break;
            
        default:
            break;
    }
    
    return color;
}

//256x256 pixels, 32x32 tiles (each tile 8x8 pixels)
//160x144 pixels displayed (20x18) tiles
static void RenderTiles()
{
    if(!frameBuffer)
        return;
    
    uchar lcdc = gpu.lcdc;
    uchar scrollx = gpu.scroll_x;
    uchar scrolly = gpu.scroll_y;
    uchar scanline = gpu.scanline;
    ushort bgTilemapLocation = 0x9800;
    ushort tileDataLocation = 0x8800;
    uchar useUnsigned = 0;
    uchar useWindow = fetchBit(&lcdc, kGBLCDCWindwDisplayEnableBit);
    uchar windowx = gpu.window_x;
    uchar windowy = gpu.window_y;
    
    
    //tile data table location
    if(fetchBit(&lcdc, kGBLCDCBGWindwTDBit) == 1)
    {
        tileDataLocation = 0x8000;
        useUnsigned = 1;
    }
    
    
    if(useWindow)
    {
        if(fetchBit(&lcdc, kGBLCDCWindowTileMapDisplayBit) == 0)
            bgTilemapLocation = 0x9800;
        else
            bgTilemapLocation = 0x9c00;
    } else
    {
        if(fetchBit(&lcdc, kGBLCDCBGTileMapDisplayBit) == 0)
            bgTilemapLocation = 0x9800;
        else
            bgTilemapLocation = 0x9c00;
    }
    
    ushort yPos = scrolly + scanline;
    
    if(useWindow)
        yPos = scanline - windowy;
    
    //data is laid out as 32 rows, filled by 32 bytes
    //32*8 = 256 = max y, yPos/8 * 32 = currentTile
    ushort tileRow = (((uchar)(yPos/8))*32); //get row location
    
    for(uchar x = 0; x < 160; x++)
    {
        uchar xPos = scrollx + x;
        
        if(useWindow && x >= windowx)
            xPos = x - windowx;
        
        ushort tileCol = xPos/8;
        
        short tileNum = 0;
        
        ushort tileIdentifier = bgTilemapLocation + tileRow + tileCol;
        if(useUnsigned)
            tileNum = (uchar)readByte(tileIdentifier);
        else
            tileNum = (signed char)readByte(tileIdentifier);
        
        ushort tilePalleteLocation = tileDataLocation;
        if(useUnsigned)
            tilePalleteLocation += tileNum*16; //each tile is 16 bytes
        else
            tilePalleteLocation += ((tileNum + 128) * 16); //account for signed value
        
        uchar verticalLine = yPos % 8; //what line of the tile should we draw?
        ushort finalPixelDataLocation = tilePalleteLocation + (verticalLine * 2);
        
        uchar b1 = readByte(finalPixelDataLocation);
        uchar b2 = readByte(finalPixelDataLocation + 1);
        
        int colorBit = xPos % 8; //get what pixel we need from the current tile
        //invert it
        colorBit -= 7;
        colorBit *= -1;
        
        uchar colorValue = (fetchBit(&b2, colorBit & 0xff) << 1) + fetchBit(&b1, colorBit & 0xff);
        
        GB_COLOR color = GetColor(colorValue, kGBBGWindowPalleteDataLocation);
        
        uchar r, g, b;
        
        switch (color) {
            case GB_WHITE:
                r = g = b = 255;
                break;
                
            case GB_LIGHT_GRAY:
                r = g = b = 211;
                break;
                
            case GB_DARK_GRAY:
                r = g = b = 169;
                break;
                
            case GB_BLACK:
                r = g = b = 0;
                break;
                
            default:
                break;
        }
        
        if(x > 0 && x < 160 && scanline > 0 && scanline < 144)
        {
            frameBuffer[scanline][x][0] = r;
            frameBuffer[scanline][x][1] = g;
            frameBuffer[scanline][x][2] = b;
        }
    }
    
}

//40 sprites
//8x8 or 8x16 in size, 10 per scanline
static void RenderSprites()
{
    ushort spriteDataLocation = 0x8000;
    ushort spriteAttributeLocation = 0xFE00;
    uchar scanline = gpu.scanline;
    uchar useDoubleHeight = fetchBit(&gpu.lcdc, kGBLCDCOBJSizeBit);
    
    uchar height = 8;
    
    for(int i = 0; i < 40; i++)
    {
        ushort baseAdress = spriteAttributeLocation + (i*4);
        uchar yPos = readByte(baseAdress) - 16;
        uchar xPos = readByte(baseAdress + 1) - 8;
        uchar patternNumber = readByte(baseAdress + 2);
        uchar flags = readByte(baseAdress + 3);
        
        if(useDoubleHeight) {//reset pattern bit0 and double height
            setBit(&patternNumber, 0, 0);
            height*=2;
        }
        
        uchar priority = fetchBit(&flags, 7);
        uchar flipY = fetchBit(&flags, 6);
        uchar flipX = fetchBit(&flags, 5);
        uchar palleteNum = fetchBit(&flags, 4);
        
        ushort paletteDataLocation = (palleteNum == 1) ? kGBOBJPaletteDataLocation0 : kGBOBJPaletteDataLocation1;
        
        if(scanline > yPos && scanline < (yPos + height))
        {
            ushort spritePalleteLocation = spriteDataLocation + (patternNumber * 16);
            
            int line = scanline - yPos;
            if(flipY)
            {
                line -= height;
                line *= -1;
            }
            
            ushort dataLocation = spritePalleteLocation + (line * 2);
            uchar b1 = readByte(dataLocation);
            uchar b2 = readByte(dataLocation+1);
            
            for(int x = 7; x >= 0; x--)
            {
                int colorBit = x;
                if(flipX)
                {

                    colorBit-= 7;
                    colorBit*= -1;
                }
                
                ushort colorNum = (fetchBit(&b2, colorBit) << 1) + fetchBit(&b1, colorBit);
                
                GB_COLOR color = GetColor(colorNum, paletteDataLocation);
                
                if(color == GB_WHITE)
                    continue;
                
                uchar r, g, b;
                
                switch (color) {
                    case GB_WHITE:
                        r = g = b = 255;
                        break;
                        
                    case GB_LIGHT_GRAY:
                        r = g = b = 211;
                        break;
                        
                    case GB_DARK_GRAY:
                        r = g = b = 169;
                        break;
                        
                    case GB_BLACK:
                        r = g = b = 0;
                        break;
                        
                    default:
                        break;
                }
                
                int xPix = 0 - x;
                xPix += 7;
                
                int pixel = xPos + xPix;
                
                if(scanline > 0 && scanline < 144 && pixel > 0 && pixel < 160)
                {
                    if(priority)
                        if(frameBuffer[scanline][pixel][0] != 255 ||
                           frameBuffer[scanline][pixel][1] != 255 ||
                           frameBuffer[scanline][pixel][2] != 255)
                            continue;
                    
                    frameBuffer[scanline][pixel][0] = r;
                    frameBuffer[scanline][pixel][1] = g;
                    frameBuffer[scanline][pixel][2] = b;
                }
            }
        }
    }
}

static void DrawScanline()
{
    if(!frameBuffer)
        return;
    
    if(fetchBit(&gpu.lcdc, kGBLCDCBGDisplayEnableBit) == 1)
        RenderTiles();
    if(fetchBit(&gpu.lcdc, kGBLCDCOBJEnableBit) == 1)
        RenderSprites();
}

void gb_gpu_step()
{
    //assert(cpu != NULL);
    
    UpdateLCDStatus();
    
    if(gb_gpu_is_display_enabled() == 0)
        return;
    
    
    gpu.ticks -= (int)cpu->cycles;
    if(gpu.ticks <= 0)
    {
        gpu.scanline++;
        
        gpu.ticks = 456;
        
        if(gpu.scanline == 144) //vblank
        {
            gb_interrupt_request(kGBIFVBlank);
        } else if(gpu.scanline > 153)
        {
            gpu.scanline = 0;
        } else if(gpu.scanline < 144)
        {
            DrawScanline();
        }
    }
    
}
