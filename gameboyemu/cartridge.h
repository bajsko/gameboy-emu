//
//  cartridge.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-12.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef cartridge_h
#define cartridge_h

///The offset to the cartridge title in the ROM
static const ushort kGBCartridgeTitleOffset = 0x134;
///The device type flag offset in the ROM
static const ushort kGBDeviceTypeOffset = 0x143;
///The cartridge type flag offset in the ROM
static const ushort kGBCartridgeTypeOffset = 0x147;
///The RAM size offset in the ROM
static const ushort kGBROMSizeOffset = 0x148;
///The RAM size offset in the ROM
static const ushort kGBRAMSizeOffset = 0x149;

///The length of a cartridge title
static const char kGBCartridgeTitleLength = 16;


#endif /* cartridge_h */
