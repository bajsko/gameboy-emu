//
//  display.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-20.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include "display.h"
#include <stdlib.h>
#include "matrix.h"

static void setupTexture(unsigned char framebuffer[][GB_RES_X][3])
{
    for(int y = 0; y < GB_RES_Y; y++)
    {
        for(int x = 0; x < GB_RES_X; x++)
        {
            framebuffer[y][x][0] = framebuffer[y][x][1] = framebuffer[y][x][2] = 0;
        }
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, GB_RES_X, GB_RES_Y, 0, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)framebuffer);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    
    glEnable(GL_TEXTURE_2D);
}

display_t* display_make()
{
    display_t* display = malloc(sizeof(display_t));
    if(!display)
    {
        return NULL;
    }
    
    int ok = 0;
    shader_t* shader = shader_loadFromFile("vertex.shader", "fragment.shader", &ok);
    if(!shader)
    {
        free(display);
        return NULL;
    }
    
    mesh_t* mesh = mesh_make_quad(GB_SCREEN_X, GB_SCREEN_Y, shader);
    if(!mesh)
    {
        shader_delete(shader);
        free(display);
        return NULL;
    }
    
    float projectionMatrix[16];
    float transformMatrix[16];
    float scaleMatrix[16];
    
    mat_set_mat2d(projectionMatrix, GB_SCREEN_X, GB_SCREEN_Y);
    mat_translate(transformMatrix, GB_SCREEN_X/2-1, GB_SCREEN_Y/2-1, 0); //move it to the top-left corner
    mat_scale(scaleMatrix, 1);
    
    mesh_set_active(mesh);
    
    shader_uniformmat4f("projectionMatrix", projectionMatrix, shader);
    shader_uniformmat4f("scaleMatrix", scaleMatrix, shader);
    shader_uniformmat4f("transformMatrix", transformMatrix, shader);
    
    display->shader = shader;
    display->quad = mesh;
    
    setupTexture(display->framebuffer);
    
    return display;
}

void display_update(display_t* display)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glTexSubImage2D(GL_TEXTURE_2D, 0 ,0, 0, GB_RES_X, GB_RES_Y, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)display->framebuffer);
    mesh_render(display->quad);
}

void display_delete(display_t* display)
{
    shader_delete(display->shader);
    mesh_delete(display->quad);
    free(display);
}