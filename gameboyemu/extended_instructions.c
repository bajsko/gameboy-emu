//
//  extended_instructions.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-30.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <assert.h>

#include "extended_instructions.h"
#include "gbutil.h"
#include "memory.h"

static cpu_t* cpu = NULL;

static const ext_command_t extendedCommandTable[256] =
{
    {"rlc b", rlc_b, 8},
    {"rlc c", rlc_c, 8},
    {"rlc d", rlc_d, 8},
    {"rlc e", rlc_e, 8},
    {"rlc h", rlc_h, 8},
    {"rlc l", rlc_l, 8},
    {"rlc (hl)", rlc_hlp, 16},
    {"rlc a", rlc_a, 8},
    {"rrc b", rrc_b, 8},
    {"rrc c", rrc_c, 8},
    {"rrc d", rrc_d, 8},
    {"rrc e", rrc_e, 8},
    {"rrc h", rrc_h, 8},
    {"rrc l", rrc_l, 8},
    {"rrc (hl)", rrc_hlp, 16},
    {"rrc a", rrc_a, 8},
    {"rl b", rl_b, 8},
    {"rl c", rl_c, 8},
    {"rl d", rl_d, 8},
    {"rl e", rl_e, 8},
    {"rl h", rl_h, 8},
    {"rl l", rl_l, 8},
    {"rl (hl)", rl_hlp, 16},
    {"rl a", rl_a, 8},
    {"rr b", rr_b, 8},
    {"rr c", rr_c, 8},
    {"rr d", rr_d, 8},
    {"rr e", rr_e, 8},
    {"rr h", rr_h, 8},
    {"rr l", rr_l, 8},
    {"rr (hl)", rr_hlp, 16},
    {"rr a", rr_a, 8},
    {"sla b", sla_b, 8},
    {"sla c", sla_c, 8},
    {"sla d", sla_d, 8},
    {"sla e", sla_e, 8},
    {"sla h", sla_h, 8},
    {"sla l", sla_l, 8},
    {"sla (hl)", sla_hlp, 16},
    {"sla a", sla_a, 8},
    {"sra b", sra_b, 8},
    {"sra c", sra_c, 8},
    {"sra d", sra_d, 8},
    {"sra e", sra_e, 8},
    {"sra h", sra_h, 8},
    {"sra l", sra_l, 8},
    {"sra (hl)", sra_hlp, 16},
    {"sra a", sra_a, 8},
    {"swap b", swap_b, 8},
    {"swap c", swap_c, 8},
    {"swap d", swap_d, 8},
    {"swap e", swap_e, 8},
    {"swap h", swap_h, 8},
    {"swap l", swap_l, 8},
    {"swap (hl)", swap_hlp, 16},
    {"swap a", swap_a, 8},
    {"srl b", srl_b, 8},
    {"srl c", srl_c, 8},
    {"srl d", srl_d, 8},
    {"srl e", srl_e, 8},
    {"srl h", srl_h, 8},
    {"srl l", srl_l, 8},
    {"srl (hl)", srl_hlp, 16},
    {"srl a", srl_a, 8},
    {"bit_0 b", bit_0_b, 8},
    {"bit_0 c", bit_0_c, 8},
    {"bit_0 d", bit_0_d, 8},
    {"bit_0 e", bit_0_e, 8},
    {"bit_0 h", bit_0_h, 8},
    {"bit_0 l", bit_0_l, 8},
    {"bit_0 (hl)", bit_0_hlp, 16},
    {"bit_0 a", bit_0_a, 8},
    {"bit_1 b", bit_1_b, 8},
    {"bit_1 c", bit_1_c, 8},
    {"bit_1 d", bit_1_d, 8},
    {"bit_1 e", bit_1_e, 8},
    {"bit_1 h", bit_1_h, 8},
    {"bit_1 l", bit_1_l, 8},
    {"bit_1 (hl)", bit_1_hlp, 16},
    {"bit_1 a", bit_1_a, 8},
    {"bit_2 b", bit_2_b, 8},
    {"bit_2 c", bit_2_c, 8},
    {"bit_2 d", bit_2_d, 8},
    {"bit_2 e", bit_2_e, 8},
    {"bit_2 h", bit_2_h, 8},
    {"bit_2 l", bit_2_l, 8},
    {"bit_2 (hl)", bit_2_hlp, 16},
    {"bit_2 a", bit_2_a, 8},
    {"bit_3 b", bit_3_b, 8},
    {"bit_3 c", bit_3_c, 8},
    {"bit_3 d", bit_3_d, 8},
    {"bit_3 e", bit_3_e, 8},
    {"bit_3 h", bit_3_h, 8},
    {"bit_3 l", bit_3_l, 8},
    {"bit_3 (hl)", bit_3_hlp, 16},
    {"bit_3 a", bit_3_a, 8},
    {"bit_4 b", bit_4_b, 8},
    {"bit_4 c", bit_4_c, 8},
    {"bit_4 d", bit_4_d, 8},
    {"bit_4 e", bit_4_e, 8},
    {"bit_4 h", bit_4_h, 8},
    {"bit_4 l", bit_4_l, 8},
    {"bit_4 (hl)", bit_4_hlp, 16},
    {"bit_4 a", bit_4_a, 8},
    {"bit_5 b", bit_5_b, 8},
    {"bit_5 c", bit_5_c, 8},
    {"bit_5 d", bit_5_d, 8},
    {"bit_5 e", bit_5_e, 8},
    {"bit_5 h", bit_5_h, 8},
    {"bit_5 l", bit_5_l, 8},
    {"bit_5 (hl)", bit_5_hlp, 16},
    {"bit_5 a", bit_5_a, 8},
    {"bit_6 b", bit_6_b, 8},
    {"bit_6 c", bit_6_c, 8},
    {"bit_6 d", bit_6_d, 8},
    {"bit_6 e", bit_6_e, 8},
    {"bit_6 h", bit_6_h, 8},
    {"bit_6 l", bit_6_l, 8},
    {"bit_6 (hl)", bit_6_hlp, 16},
    {"bit_6 a", bit_6_a, 8},
    {"bit_7 b", bit_7_b, 8},
    {"bit_7 c", bit_7_c, 8},
    {"bit_7 d", bit_7_d, 8},
    {"bit_7 e", bit_7_e, 8},
    {"bit_7 h", bit_7_h, 8},
    {"bit_7 l", bit_7_l, 8},
    {"bit_7 (hl)", bit_7_hlp, 16},
    {"bit_7 a", bit_7_a, 8},
    {"res_0 b", res_0_b, 8},
    {"res_0 c", res_0_c, 8},
    {"res_0 d", res_0_d, 8},
    {"res_0 e", res_0_e, 8},
    {"res_0 h", res_0_h, 8},
    {"res_0 l", res_0_l, 8},
    {"res_0 (hl)", res_0_hlp, 16},
    {"res_0 a", res_0_a, 8},
    {"res_1 b", res_1_b, 8},
    {"res_1 c", res_1_c, 8},
    {"res_1 d", res_1_d, 8},
    {"res_1 e", res_1_e, 8},
    {"res_1 h", res_1_h, 8},
    {"res_1 l", res_1_l, 8},
    {"res_1 (hl)", res_1_hlp, 16},
    {"res_1 a", res_1_a, 8},
    {"res_2 b", res_2_b, 8},
    {"res_2 c", res_2_c, 8},
    {"res_2 d", res_2_d, 8},
    {"res_2 e", res_2_e, 8},
    {"res_2 h", res_2_h, 8},
    {"res_2 l", res_2_l, 8},
    {"res_2 (hl)", res_2_hlp, 16},
    {"res_2 a", res_2_a, 8},
    {"res_3 b", res_3_b, 8},
    {"res_3 c", res_3_c, 8},
    {"res_3 d", res_3_d, 8},
    {"res_3 e", res_3_e, 8},
    {"res_3 h", res_3_h, 8},
    {"res_3 l", res_3_l, 8},
    {"res_3 (hl)", res_3_hlp, 16},
    {"res_3 a", res_3_a, 8},
    {"res_4 b", res_4_b, 8},
    {"res_4 c", res_4_c, 8},
    {"res_4 d", res_4_d, 8},
    {"res_4 e", res_4_e, 8},
    {"res_4 h", res_4_h, 8},
    {"res_4 l", res_4_l, 8},
    {"res_4 (hl)", res_4_hlp, 16},
    {"res_4 a", res_4_a, 8},
    {"res_5 b", res_5_b, 8},
    {"res_5 c", res_5_c, 8},
    {"res_5 d", res_5_d, 8},
    {"res_5 e", res_5_e, 8},
    {"res_5 h", res_5_h, 8},
    {"res_5 l", res_5_l, 8},
    {"res_5 (hl)", res_5_hlp, 16},
    {"res_5 a", res_5_a, 8},
    {"res_6 b", res_6_b, 8},
    {"res_6 c", res_6_c, 8},
    {"res_6 d", res_6_d, 8},
    {"res_6 e", res_6_e, 8},
    {"res_6 h", res_6_h, 8},
    {"res_6 l", res_6_l, 8},
    {"res_6 (hl)", res_6_hlp, 16},
    {"res_6 a", res_6_a, 8},
    {"res_7 b", res_7_b, 8},
    {"res_7 c", res_7_c, 8},
    {"res_7 d", res_7_d, 8},
    {"res_7 e", res_7_e, 8},
    {"res_7 h", res_7_h, 8},
    {"res_7 l", res_7_l, 8},
    {"res_7 (hl)", res_7_hlp, 16},
    {"res_7 a", res_7_a, 8},
    {"set_0 b", set_0_b, 8},
    {"set_0 c", set_0_c, 8},
    {"set_0 d", set_0_d, 8},
    {"set_0 e", set_0_e, 8},
    {"set_0 h", set_0_h, 8},
    {"set_0 l", set_0_l, 8},
    {"set_0 (hl)", set_0_hlp, 16},
    {"set_0 a", set_0_a, 8},
    {"set_1 b", set_1_b, 8},
    {"set_1 c", set_1_c, 8},
    {"set_1 d", set_1_d, 8},
    {"set_1 e", set_1_e, 8},
    {"set_1 h", set_1_h, 8},
    {"set_1 l", set_1_l, 8},
    {"set_1 (hl)", set_1_hlp, 16},
    {"set_1 a", set_1_a, 8},
    {"set_2 b", set_2_b, 8},
    {"set_2 c", set_2_c, 8},
    {"set_2 d", set_2_d, 8},
    {"set_2 e", set_2_e, 8},
    {"set_2 h", set_2_h, 8},
    {"set_2 l", set_2_l, 8},
    {"set_2 (hl)", set_2_hlp, 16},
    {"set_2 a", set_2_a, 8},
    {"set_3 b", set_3_b, 8},
    {"set_3 c", set_3_c, 8},
    {"set_3 d", set_3_d, 8},
    {"set_3 e", set_3_e, 8},
    {"set_3 h", set_3_h, 8},
    {"set_3 l", set_3_l, 8},
    {"set_3 (hl)", set_3_hlp, 16},
    {"set_3 a", set_3_a, 8},
    {"set_4 b", set_4_b, 8},
    {"set_4 c", set_4_c, 8},
    {"set_4 d", set_4_d, 8},
    {"set_4 e", set_4_e, 8},
    {"set_4 h", set_4_h, 8},
    {"set_4 l", set_4_l, 8},
    {"set_4 (hl)", set_4_hlp, 16},
    {"set_4 a", set_4_a, 8},
    {"set_5 b", set_5_b, 8},
    {"set_5 c", set_5_c, 8},
    {"set_5 d", set_5_d, 8},
    {"set_5 e", set_5_e, 8},
    {"set_5 h", set_5_h, 8},
    {"set_5 l", set_5_l, 8},
    {"set_5 (hl)", set_5_hlp, 16},
    {"set_5 a", set_5_a, 8},
    {"set_6 b", set_6_b, 8},
    {"set_6 c", set_6_c, 8},
    {"set_6 d", set_6_d, 8},
    {"set_6 e", set_6_e, 8},
    {"set_6 h", set_6_h, 8},
    {"set_6 l", set_6_l, 8},
    {"set_6 (hl)", set_6_hlp, 16},
    {"set_6 a", set_6_a, 8},
    {"set_7 b", set_7_b, 8},
    {"set_7 c", set_7_c, 8},
    {"set_7 d", set_7_d, 8},
    {"set_7 e", set_7_e, 8},
    {"set_7 h", set_7_h, 8},
    {"set_7 l", set_7_l, 8},
    {"set_7 (hl)", set_7_hlp, 16},
    {"set_7 a", set_7_a, 8},
    
};

void gb_extended_instructions_attach(cpu_t* l_cpu)
{
    assert(l_cpu != NULL);
    assert(cpu == NULL);
    
    cpu = l_cpu;
    gb_log("Extended instructions: attached to CPU!");
}

void gb_extended_instructions_detach()
{
    assert(cpu != NULL);
    
    cpu = NULL;
    gb_log("Extended instructions: detached from CPU!");
}

void gb_extended(uchar value)
{
    ext_command_t command = extendedCommandTable[value];
    ((void(*)(void))command.action)();
    cpu->cycles = command.cycles;
    
    //gb_log("Executed CB instruction: %s", command.name);
}

//opcodes

static uchar rlc(uchar value)
{
    uchar bit7 = fetchBit(&value, 7);
    
    if(bit7 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value <<= 1;
    value |= (bit7 << 7);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar rrc(uchar value)
{
    uchar bit0 = fetchBit(&value, 0);
    
    if(bit0 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value >>= 1;
    setBit(&value, 7, bit0);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar rl(uchar value)
{
    uchar carryflag = gb_read_sf(kGBSFCarry);
    uchar bit7 = fetchBit(&value, 7);
    
    if(bit7 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value <<= 1;
    value |= carryflag;
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar rr(uchar value)
{
    uchar carryflag = gb_read_sf(kGBSFCarry);
    uchar bit0 = fetchBit(&value, 0);
    
    if(bit0 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value >>= 1;
    setBit(&value, 7, carryflag);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar sla(uchar value)
{
    uchar bit7 = fetchBit(&value, 7);
    if(bit7 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value <<= 1;
    setBit(&value, 0, 0);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar sra(uchar value)
{
    uchar bit0 = fetchBit(&value, 0);
    
    if(bit0 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value = (value & 0x80) | (value >> 1);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static uchar swap(uchar value)
{
    value = ((value & 0x0f) << 4) | ((value & 0xf0) >> 4);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    gb_unset_sf(kGBSFCarry);
    
    return value;
}

static uchar srl(uchar value)
{
    uchar bit0 = fetchBit(&value, 0);
    
    if(bit0 == 0x0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    value >>= 1;
    setBit(&value, 7, 0);
    
    if(value == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
    return value;
}

static void bit(uchar bit, uchar value)
{
    if(value & (1 << bit)) gb_unset_sf(kGBSFZero);
    else gb_set_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_set_sf(kGBSFHalfCarry);
}

static uchar res(uchar bit, uchar value)
{
    setBit(&value, bit, 0);
    return value;
}

static uchar set(uchar bit, uchar value)
{
    setBit(&value, bit, 1);
    return value;
}

void rlc_b()
{
    cpu->regs.B = rlc(cpu->regs.B);
}

void rlc_c()
{
    cpu->regs.C = rlc(cpu->regs.C);
}

void rlc_d()
{
    cpu->regs.D = rlc(cpu->regs.D);
}

void rlc_e()
{
    cpu->regs.E = rlc(cpu->regs.E);
}

void rlc_h()
{
    cpu->regs.H = rlc(cpu->regs.H);
}

void rlc_l()
{
    cpu->regs.L = rlc(cpu->regs.L);
}

void rlc_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, rlc(readByte(adress)));
}

void rlc_a()
{
    cpu->regs.A = rlc(cpu->regs.A);
}

void rrc_b()
{
    cpu->regs.B = rrc(cpu->regs.B);
}

void rrc_c()
{
    cpu->regs.C = rrc(cpu->regs.C);
}

void rrc_d()
{
    cpu->regs.D = rrc(cpu->regs.D);
}

void rrc_e()
{
    cpu->regs.E = rrc(cpu->regs.E);
}

void rrc_h()
{
    cpu->regs.H = rrc(cpu->regs.H);
}

void rrc_l()
{
    cpu->regs.L = rrc(cpu->regs.L);
}

void rrc_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, rrc(readByte(adress)));
}

void rrc_a()
{
    cpu->regs.A = rrc(cpu->regs.A);
}
//0x1
void rl_b()
{
    cpu->regs.B = rl(cpu->regs.B);
}

void rl_c()
{
    cpu->regs.C = rl(cpu->regs.C);
}

void rl_d()
{
    cpu->regs.D = rl(cpu->regs.D);
}

void rl_e()
{
    cpu->regs.E = rl(cpu->regs.E);
}

void rl_h()
{
    cpu->regs.H = rl(cpu->regs.H);
}

void rl_l()
{
    cpu->regs.L = rl(cpu->regs.L);
}

void rl_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, rl(readByte(adress)));
}

void rl_a()
{
    cpu->regs.A = rl(cpu->regs.A);
}
void rr_b()
{
    cpu->regs.B = rr(cpu->regs.B);
}

void rr_c()
{
    cpu->regs.C = rr(cpu->regs.C);
}

void rr_d()
{
    cpu->regs.D = rr(cpu->regs.D);
}

void rr_e()
{
    cpu->regs.E = rr(cpu->regs.E);
}

void rr_h()
{
    cpu->regs.H = rr(cpu->regs.H);
}

void rr_l()
{
    cpu->regs.L = rr(cpu->regs.L);
}

void rr_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, rr(readByte(adress)));
}

void rr_a()
{
    cpu->regs.A = rr(cpu->regs.A);
}
//0x2
void sla_b()
{
    cpu->regs.B = sla(cpu->regs.B);
}

void sla_c()
{
    cpu->regs.C = sla(cpu->regs.C);
}

void sla_d()
{
    cpu->regs.D = sla(cpu->regs.D);
}

void sla_e()
{
    cpu->regs.E = sla(cpu->regs.E);
}

void sla_h()
{
    cpu->regs.H = sla(cpu->regs.H);
}

void sla_l()
{
    cpu->regs.L = sla(cpu->regs.L);
}

void sla_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, sla(readByte(adress)));
}

void sla_a()
{
    cpu->regs.A = sla(cpu->regs.A);
}
void sra_b()
{
    cpu->regs.B = sra(cpu->regs.B);
}

void sra_c()
{
    cpu->regs.C = sra(cpu->regs.C);
}

void sra_d()
{
    cpu->regs.D = sra(cpu->regs.D);
}

void sra_e()
{
    cpu->regs.E = sra(cpu->regs.E);
}

void sra_h()
{
    cpu->regs.H = sra(cpu->regs.H);
}

void sra_l()
{
    cpu->regs.L = sra(cpu->regs.L);
}

void sra_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, sra(readByte(adress)));
}

void sra_a()
{
    cpu->regs.A = sra(cpu->regs.A);
}
//0x3
void swap_b()
{
    cpu->regs.B = swap(cpu->regs.B);
}

void swap_c()
{
    cpu->regs.C = swap(cpu->regs.C);
}

void swap_d()
{
    cpu->regs.D = swap(cpu->regs.D);
}

void swap_e()
{
    cpu->regs.E = swap(cpu->regs.E);
}

void swap_h()
{
    cpu->regs.H = swap(cpu->regs.H);
}

void swap_l()
{
    cpu->regs.L = swap(cpu->regs.L);
}

void swap_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, swap(readByte(adress)));
}

void swap_a()
{
    cpu->regs.A = swap(cpu->regs.A);
}
void srl_b()
{
    cpu->regs.B = srl(cpu->regs.B);
}

void srl_c()
{
    cpu->regs.C = srl(cpu->regs.C);
}

void srl_d()
{
    cpu->regs.D = srl(cpu->regs.D);
}

void srl_e()
{
    cpu->regs.E = srl(cpu->regs.E);
}

void srl_h()
{
    cpu->regs.H = srl(cpu->regs.H);
}

void srl_l()
{
    cpu->regs.L = srl(cpu->regs.L);
}

void srl_hlp()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, srl(readByte(adress)));
}

void srl_a()
{
    cpu->regs.A = srl(cpu->regs.A);
}
//0x4
void bit_0_b() { bit(0, cpu->regs.B); }
void bit_0_c() { bit(0, cpu->regs.C); }
void bit_0_d() { bit(0, cpu->regs.D); }
void bit_0_e() { bit(0, cpu->regs.E); }
void bit_0_h() { bit(0, cpu->regs.H); }
void bit_0_l() { bit(0, cpu->regs.L); }
void bit_0_hlp() { bit(0, readByte(cpu->regs.HL)); }
void bit_0_a() { bit(0, cpu->regs.A); }
void bit_1_b() { bit(1, cpu->regs.B); }
void bit_1_c() { bit(1, cpu->regs.C); }
void bit_1_d() { bit(1, cpu->regs.D); }
void bit_1_e() { bit(1, cpu->regs.E); }
void bit_1_h() { bit(1, cpu->regs.H); }
void bit_1_l() { bit(1, cpu->regs.L); }
void bit_1_hlp() { bit(1, readByte(cpu->regs.HL)); }
void bit_1_a() { bit(1, cpu->regs.A); }
//0x5
void bit_2_b() { bit(2, cpu->regs.B); }
void bit_2_c() { bit(2, cpu->regs.C); }
void bit_2_d() { bit(2, cpu->regs.D); }
void bit_2_e() { bit(2, cpu->regs.E); }
void bit_2_h() { bit(2, cpu->regs.H); }
void bit_2_l() { bit(2, cpu->regs.L); }
void bit_2_hlp() { bit(2, readByte(cpu->regs.HL)); }
void bit_2_a() { bit(2, cpu->regs.A); }
void bit_3_b() { bit(3, cpu->regs.B); }
void bit_3_c() { bit(3, cpu->regs.C); }
void bit_3_d() { bit(3, cpu->regs.D); }
void bit_3_e() { bit(3, cpu->regs.E); }
void bit_3_h() { bit(3, cpu->regs.H); }
void bit_3_l() { bit(3, cpu->regs.L); }
void bit_3_hlp() { bit(3, readByte(cpu->regs.HL)); }
void bit_3_a() { bit(3, cpu->regs.A); }
//0x6
void bit_4_b() { bit(4, cpu->regs.B); }
void bit_4_c() { bit(4, cpu->regs.C); }
void bit_4_d() { bit(4, cpu->regs.D); }
void bit_4_e() { bit(4, cpu->regs.E); }
void bit_4_h() { bit(4, cpu->regs.H); }
void bit_4_l() { bit(4, cpu->regs.L); }
void bit_4_hlp() { bit(4, readByte(cpu->regs.HL)); }
void bit_4_a() { bit(4, cpu->regs.A); }
void bit_5_b() { bit(5, cpu->regs.B); }
void bit_5_c() { bit(5, cpu->regs.C); }
void bit_5_d() { bit(5, cpu->regs.D); }
void bit_5_e() { bit(5, cpu->regs.E); }
void bit_5_h() { bit(5, cpu->regs.H); }
void bit_5_l() { bit(5, cpu->regs.L); }
void bit_5_hlp() { bit(5, readByte(cpu->regs.HL)); }
void bit_5_a() { bit(5, cpu->regs.A); }
//0x7
void bit_6_b() { bit(6, cpu->regs.B); }
void bit_6_c() { bit(6, cpu->regs.C); }
void bit_6_d() { bit(6, cpu->regs.D); }
void bit_6_e() { bit(6, cpu->regs.E); }
void bit_6_h() { bit(6, cpu->regs.H); }
void bit_6_l() { bit(6, cpu->regs.L); }
void bit_6_hlp() { bit(6, readByte(cpu->regs.HL)); }
void bit_6_a() { bit(6, cpu->regs.A); }
void bit_7_b() { bit(7, cpu->regs.B); }
void bit_7_c() { bit(7, cpu->regs.C); }
void bit_7_d() { bit(7, cpu->regs.D); }
void bit_7_e() { bit(7, cpu->regs.E); }
void bit_7_h() { bit(7, cpu->regs.H); }
void bit_7_l() { bit(7, cpu->regs.L); }
void bit_7_hlp() { bit(7, readByte(cpu->regs.HL)); }
void bit_7_a() { bit(7, cpu->regs.A); }
//0x8
void res_0_b() { cpu->regs.B = res(0, cpu->regs.B); }
void res_0_c() { cpu->regs.C = res(0, cpu->regs.C); }
void res_0_d() { cpu->regs.D = res(0, cpu->regs.D); }
void res_0_e() { cpu->regs.E = res(0, cpu->regs.E); }
void res_0_h() { cpu->regs.H = res(0, cpu->regs.H); }
void res_0_l() { cpu->regs.L = res(0, cpu->regs.L); }
void res_0_hlp() { writeByte(cpu->regs.HL, res(0, readByte(cpu->regs.HL))); }
void res_0_a() { cpu->regs.A = res(0, cpu->regs.A); }
void res_1_b() { cpu->regs.B = res(1, cpu->regs.B); }
void res_1_c() { cpu->regs.C = res(1, cpu->regs.C); }
void res_1_d() { cpu->regs.D = res(1, cpu->regs.D); }
void res_1_e() { cpu->regs.E = res(1, cpu->regs.E); }
void res_1_h() { cpu->regs.H = res(1, cpu->regs.H); }
void res_1_l() { cpu->regs.L = res(1, cpu->regs.L); }
void res_1_hlp() { writeByte(cpu->regs.HL, res(1, readByte(cpu->regs.HL))); }
void res_1_a() { cpu->regs.A = res(1, cpu->regs.A); }
//0x9
void res_2_b() { cpu->regs.B = res(2, cpu->regs.B); }
void res_2_c() { cpu->regs.C = res(2, cpu->regs.C); }
void res_2_d() { cpu->regs.D = res(2, cpu->regs.D); }
void res_2_e() { cpu->regs.E = res(2, cpu->regs.E); }
void res_2_h() { cpu->regs.H = res(2, cpu->regs.H); }
void res_2_l() { cpu->regs.L = res(2, cpu->regs.L); }
void res_2_hlp() { writeByte(cpu->regs.HL, res(2, readByte(cpu->regs.HL))); }
void res_2_a() { cpu->regs.A = res(2, cpu->regs.A); }
void res_3_b() { cpu->regs.B = res(3, cpu->regs.B); }
void res_3_c() { cpu->regs.C = res(3, cpu->regs.C); }
void res_3_d() { cpu->regs.D = res(3, cpu->regs.D); }
void res_3_e() { cpu->regs.E = res(3, cpu->regs.E); }
void res_3_h() { cpu->regs.H = res(3, cpu->regs.H); }
void res_3_l() { cpu->regs.L = res(3, cpu->regs.L); }
void res_3_hlp() { writeByte(cpu->regs.HL, res(3, readByte(cpu->regs.HL))); }
void res_3_a() { cpu->regs.A = res(3, cpu->regs.A); }
//0xA
void res_4_b() { cpu->regs.B = res(4, cpu->regs.B); }
void res_4_c() { cpu->regs.C = res(4, cpu->regs.C); }
void res_4_d() { cpu->regs.D = res(4, cpu->regs.D); }
void res_4_e() { cpu->regs.E = res(4, cpu->regs.E); }
void res_4_h() { cpu->regs.H = res(4, cpu->regs.H); }
void res_4_l() { cpu->regs.L = res(4, cpu->regs.L); }
void res_4_hlp() { writeByte(cpu->regs.HL, res(4, readByte(cpu->regs.HL))); }
void res_4_a() { cpu->regs.A = res(4, cpu->regs.A); }
void res_5_b() { cpu->regs.B = res(5, cpu->regs.B); }
void res_5_c() { cpu->regs.C = res(5, cpu->regs.C); }
void res_5_d() { cpu->regs.D = res(5, cpu->regs.D); }
void res_5_e() { cpu->regs.E = res(5, cpu->regs.E); }
void res_5_h() { cpu->regs.H = res(5, cpu->regs.H); }
void res_5_l() { cpu->regs.L = res(5, cpu->regs.L); }
void res_5_hlp() { writeByte(cpu->regs.HL, res(5, readByte(cpu->regs.HL))); }
void res_5_a() { cpu->regs.A = res(5, cpu->regs.A); }
//0xB
void res_6_b() { cpu->regs.B = res(6, cpu->regs.B); }
void res_6_c() { cpu->regs.C = res(6, cpu->regs.C); }
void res_6_d() { cpu->regs.D = res(6, cpu->regs.D); }
void res_6_e() { cpu->regs.E = res(6, cpu->regs.E); }
void res_6_h() { cpu->regs.H = res(6, cpu->regs.H); }
void res_6_l() { cpu->regs.L = res(6, cpu->regs.L); }
void res_6_hlp() { writeByte(cpu->regs.HL, res(6, readByte(cpu->regs.HL))); }
void res_6_a() { cpu->regs.A = res(6, cpu->regs.A); }
void res_7_b() { cpu->regs.B = res(7, cpu->regs.B); }
void res_7_c() { cpu->regs.C = res(7, cpu->regs.C); }
void res_7_d() { cpu->regs.D = res(7, cpu->regs.D); }
void res_7_e() { cpu->regs.E = res(7, cpu->regs.E); }
void res_7_h() { cpu->regs.H = res(7, cpu->regs.H); }
void res_7_l() { cpu->regs.L = res(7, cpu->regs.L); }
void res_7_hlp() { writeByte(cpu->regs.HL, res(7, readByte(cpu->regs.HL))); }
void res_7_a() { cpu->regs.A = res(7, cpu->regs.A); }
//0xC
void set_0_b() { cpu->regs.B = set(0, cpu->regs.B); }
void set_0_c() { cpu->regs.C = set(0, cpu->regs.C); }
void set_0_d() { cpu->regs.D = set(0, cpu->regs.D); }
void set_0_e() { cpu->regs.E = set(0, cpu->regs.E); }
void set_0_h() { cpu->regs.H = set(0, cpu->regs.H); }
void set_0_l() { cpu->regs.L = set(0, cpu->regs.L); }
void set_0_hlp() { writeByte(cpu->regs.HL, set(0, readByte(cpu->regs.HL))); }
void set_0_a() { cpu->regs.A = set(0, cpu->regs.A); }
void set_1_b() { cpu->regs.B = set(1, cpu->regs.B); }
void set_1_c() { cpu->regs.C = set(1, cpu->regs.C); }
void set_1_d() { cpu->regs.D = set(1, cpu->regs.D); }
void set_1_e() { cpu->regs.E = set(1, cpu->regs.E); }
void set_1_h() { cpu->regs.H = set(1, cpu->regs.H); }
void set_1_l() { cpu->regs.L = set(1, cpu->regs.L); }
void set_1_hlp() { writeByte(cpu->regs.HL, set(1, readByte(cpu->regs.HL))); }
void set_1_a() { cpu->regs.A = set(1, cpu->regs.A); }
//0xD
void set_2_b() { cpu->regs.B = set(2, cpu->regs.B); }
void set_2_c() { cpu->regs.C = set(2, cpu->regs.C); }
void set_2_d() { cpu->regs.D = set(2, cpu->regs.D); }
void set_2_e() { cpu->regs.E = set(2, cpu->regs.E); }
void set_2_h() { cpu->regs.H = set(2, cpu->regs.H); }
void set_2_l() { cpu->regs.L = set(2, cpu->regs.L); }
void set_2_hlp() { writeByte(cpu->regs.HL, set(2, readByte(cpu->regs.HL))); }
void set_2_a() { cpu->regs.A = set(2, cpu->regs.A); }
void set_3_b() { cpu->regs.B = set(3, cpu->regs.B); }
void set_3_c() { cpu->regs.C = set(3, cpu->regs.C); }
void set_3_d() { cpu->regs.D = set(3, cpu->regs.D); }
void set_3_e() { cpu->regs.E = set(3, cpu->regs.E); }
void set_3_h() { cpu->regs.H = set(3, cpu->regs.H); }
void set_3_l() { cpu->regs.L = set(3, cpu->regs.L); }
void set_3_hlp() { writeByte(cpu->regs.HL, set(3, readByte(cpu->regs.HL))); }
void set_3_a() { cpu->regs.A = set(3, cpu->regs.A); }
//0xE
void set_4_b() { cpu->regs.B = set(4, cpu->regs.B); }
void set_4_c() { cpu->regs.C = set(4, cpu->regs.C); }
void set_4_d() { cpu->regs.D = set(4, cpu->regs.D); }
void set_4_e() { cpu->regs.E = set(4, cpu->regs.E); }
void set_4_h() { cpu->regs.H = set(4, cpu->regs.H); }
void set_4_l() { cpu->regs.L = set(4, cpu->regs.L); }
void set_4_hlp() { writeByte(cpu->regs.HL, set(4, readByte(cpu->regs.HL))); }
void set_4_a() { cpu->regs.A = set(4, cpu->regs.A); }
void set_5_b() { cpu->regs.B = set(5, cpu->regs.B); }
void set_5_c() { cpu->regs.C = set(5, cpu->regs.C); }
void set_5_d() { cpu->regs.D = set(5, cpu->regs.D); }
void set_5_e() { cpu->regs.E = set(5, cpu->regs.E); }
void set_5_h() { cpu->regs.H = set(5, cpu->regs.H); }
void set_5_l() { cpu->regs.L = set(5, cpu->regs.L); }
void set_5_hlp() { writeByte(cpu->regs.HL, set(5, readByte(cpu->regs.HL))); }
void set_5_a() { cpu->regs.A = set(5, cpu->regs.A); }
//0xF
void set_6_b() { cpu->regs.B = set(6, cpu->regs.B); }
void set_6_c() { cpu->regs.C = set(6, cpu->regs.C); }
void set_6_d() { cpu->regs.D = set(6, cpu->regs.D); }
void set_6_e() { cpu->regs.E = set(6, cpu->regs.E); }
void set_6_h() { cpu->regs.H = set(6, cpu->regs.H); }
void set_6_l() { cpu->regs.L = set(6, cpu->regs.L); }
void set_6_hlp() { writeByte(cpu->regs.HL, set(6, readByte(cpu->regs.HL))); }
void set_6_a() { cpu->regs.A = set(6, cpu->regs.A); }
void set_7_b() { cpu->regs.B = set(7, cpu->regs.B); }
void set_7_c() { cpu->regs.C = set(7, cpu->regs.C); }
void set_7_d() { cpu->regs.D = set(7, cpu->regs.D); }
void set_7_e() { cpu->regs.E = set(7, cpu->regs.E); }
void set_7_h() { cpu->regs.H = set(7, cpu->regs.H); }
void set_7_l() { cpu->regs.L = set(7, cpu->regs.L); }
void set_7_hlp() { writeByte(cpu->regs.HL, set(7, readByte(cpu->regs.HL))); }
void set_7_a() { cpu->regs.A = set(7, cpu->regs.A); }
