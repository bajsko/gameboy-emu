//
//  shader.c
//  gameboyemu
//
//  Code from my old multisnake project.
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include "shader.h"

#include "shader.h"
#include <string.h>
#include <stdlib.h>

/*
 =======================================================
 char* charArrayFromFile(const char* filepath)
 
 Arguments:
 const char* filepath: Filepath where file is located
 
 Purpose: Reads ALL file data into a char array.
 
 Returns:
 NULL if error occour
 char array if success
 =======================================================
 */
static char* charArrayFromFile(const char* filepath)
{
    FILE* file = fopen(filepath, "rb");
    if(!file)
        return NULL;
    
    fseek(file, 0, SEEK_END);
    int fsize = (int)ftell(file);
    fseek(file, 0, SEEK_SET);
    
    char* array = malloc(fsize * sizeof(char) + 1);
    fread(array, sizeof(char), fsize, file);
    fclose(file);
    
    array[fsize] = 0;
    
    return array;
}

/*
 =========================================================================================
 shader_t* shader_loadFromFile(const char* vertexFile, const char* fragmentFile, int* ok)
 
 Arguments:
 const char* vertexFile: char array containing vertex shader
 const char* fragmentFile: char array containing fragment shader
 int* ok: Additional informatal return. Can not be NULL.
 
 Purpose: Loads a shader_t structure from specified vertex and fragment shaders
 
 Returns:
 NULL & ok = -1 if anything fails
 shader_t structure
 =========================================================================================
 */
shader_t* shader_loadFromFile(const char* vertexFile, const char* fragmentFile, int* ok)
{
    
    //load vertex shader content and check for errors
    const char* vertexData = charArrayFromFile(vertexFile);
    if(vertexData == NULL)
    {
        printf("Error: could not read vertex shader file!\n");
        *ok = -1;
        return NULL;
    }
    
    //load fragment shader content and check for errors
    const char* fragmentData = charArrayFromFile(fragmentFile);
    if(fragmentData == NULL)
    {
        printf("Error: could not read fragment shader file!\n");
        *ok = -1;
        return NULL;
    }
    
    //create a vertex shader object
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    //assign shader source
    glShaderSource(vertexShader, 1, &vertexData, NULL);
    //compile shader
    glCompileShader(vertexShader);
    
    //check compilation status
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    
    if(status != GL_TRUE)
    {
        printf("--> Error: could not compile vertex shader!\n");
        char buffer[512];
        glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
        printf("OpenGL error: %s\n", buffer);
        *ok = -1;
        return NULL;
    }
    
    //same as above but with fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentData, NULL);
    glCompileShader(fragmentShader);
    
    status = 0;
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    
    if(status != GL_TRUE)
    {
        printf("--> Error: could not compile fragment shader!\n");
        char buffer[512];
        glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
        printf("OpenGL error: %s\n", buffer);
        *ok = -1;
        return NULL;
    }
    
    free((void*)vertexData);
    free((void*)fragmentData);
    
    //create a shader program, and then bind our vertex and fragment shaders to it.
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocationEXT(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);
    glUseProgram(0);
    
    //finally create our shader object and check for errors one last time
    shader_t* shader = malloc(sizeof(shader_t));
    if(!shader)
    {
        printf("--> Error: Could not create shader object!\n");
        *ok = -1;
        return NULL;
    }
    
    //assign shader objects
    shader->fragmentShader = fragmentShader;
    shader->vertexShader = vertexShader;
    shader->shaderProgram = shaderProgram;
    
    return shader;
}

/*
 ==================================
 void shader_bind(shader_t* shader)
 
 Arguments:
 shader_t* shader: Shader to bind.
 
 Purpose: Binds specified shader to the OpenGL rendering pipeline
 
 Returns:
 -
 ==================================
 */
void shader_bind(shader_t* shader)
{
    glUseProgram(shader->shaderProgram);
}

/*
====================================================================================================================
 int shader_set_attribute(const char* attribute_name, int num_values_per_input, int type, int normalize, int stride, ssize_t offset, shader_t* shader)
 
 Arguments:
    const char* attribute_name: Shader Attribue to set
    int num_values_per_input: Number of values per input (usually a vertex)
    int type: value type, e.g GL_FLOAT
    int normalize: Normalize input between -1 and 1?
    int stride: Gap between input from first value to next first value.
    ssize_t offset: Offset between values
    shader_t* shader: Shader to apply attribute to.
 
 Purpose: Updates an OGL attribute with specified input values.
 
 Returns:
    -1 if anything fails
    0 if success
====================================================================================================================
 */
int shader_set_attribute(const char* attribute_name, int num_values_per_input, int type, int normalize, int stride, ssize_t offset, shader_t* shader)
{
    GLint attrib = glGetAttribLocation(shader->shaderProgram, attribute_name);
    if(attrib < 0) return -1;
    
    glVertexAttribPointer(attrib, num_values_per_input, type, normalize, stride, (void*)offset);
    glEnableVertexAttribArray(attrib);
    
    return 0;
}

/*
 ============================================================================================
 int shader_uniform3f(const char* uniformName, float x, float y, float z, shader_t* shader)
 
 Arguments:
    const char* uniformName: Name of vector3 uniform to edit
    float x: X component of uniform
    floay y: Y component of uniform
    float z: Z component of uniform
    shader_t* shader: Shader containing this uniform 3f
 
 Purpose:
    Sets a uniform object (vector3) inside specified shader
    (e.g. Color component of a shader)
 
 Returns:
    -1 if anything fails
    0 if success
 ============================================================================================
 */
int shader_uniform3f(const char* uniformName, float x, float y, float z, shader_t* shader)
{
    if(!shader)
        return -1;
    
    shader_bind(shader);
    
    GLint uniformRef = glGetUniformLocation(shader->shaderProgram, uniformName);
    glUniform3f(uniformRef, x, y, z);
    
    
    shader_unbind();
    
    return 0;
}

/*
 ===================================================================================
 int shader_uniformmat4f(const char* uniformName, float* matrix, shader_t* shader)
 
 Arguments:
    const char* uniformName: Name of matrix4x4 uniform to edit
    float* matrix: Matrix to apply (DOES NOT CHECK FOR BUFFER OVERFLOW!)
    shader_t* shader: Shader containing this matrix4x4 uniform
 
 Purpose:
    Sets a uniform object (matrix 4x4) inside specified shader
    (e.g. Transformation matrix of a shader)
 
 Returns:
    -1 if anything fails
    0 if success
 ====================================================================================
 */
int shader_uniformmat4f(const char* uniformName, float* matrix, shader_t* shader)
{
    if(!matrix || !shader)
        return -1;
    
    shader_bind(shader);
    
    GLint uniformRef = glGetUniformLocation(shader->shaderProgram, uniformName);
    glUniformMatrix4fv(uniformRef, 1, GL_FALSE, matrix);
    
    shader_unbind();
    
    return 0;
}

/*
 =================================================
 void shader_unbind()
 
 Purpose:
    Unbinds this shader from the OpenGL pipeline.
 =================================================
 */
void shader_unbind()
{
    glUseProgram(0);
}

/*
 =================================================
 void shader_delete(shader_t* shader)
 
 Arguments:
    shader_t* shader: Shader to free.
 
 Purpose:
    Frees up and unbinds specified shader.
 =================================================
 */
void shader_delete(shader_t* shader)
{
    shader_unbind();
    glDeleteProgram(shader->shaderProgram);
    glDeleteShader(shader->vertexShader);
    glDeleteShader(shader->fragmentShader);
}