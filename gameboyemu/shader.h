//
//  shader.h
//  gameboyemu
//
//  Code from my old multisnake project.
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef shader_h
#define shader_h

#include <stdio.h>
#include <GLFW/glfw3.h>

typedef struct shader
{
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;
} shader_t;

shader_t* shader_loadFromFile(const char* vertexFile, const char* fragmentFile, int* ok);
//shader_t* loadDefaultShader();
void shader_bind(shader_t* shader);
void shader_unbind();
void shader_delete(shader_t* shader);

int shader_set_attribute(const char* attribute_name, int num_values_per_input, int type, int normalize, int stride, ssize_t offset, shader_t* shader);
int shader_uniform3f(const char* uniformName, float x, float y, float z, shader_t* shader);
int shader_uniformmat4f(const char* uniformName, float* matrix, shader_t* shader);

#endif /* shader_h */
