//
//  cpu.h
//  gameboyemu
//
//  The Z80 GameBoy modified CPU implementation.
//  Video RAM: 8kb
//  Main RAM: 8kb
//  Litle endian system
//
//  Created by Klas Henriksson on 2016-09-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef cpu_h
#define cpu_h

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    #define LITTLE_ENDIAN_BYTE_ORDER
#elif
    #define BIG_ENDIAN
#endif

#include <stdio.h>

#include "gbutil.h"
#include "io.h"

///C flag
static const char kGBSFCarry = 4;
///N flag
static const char kGBSFSubtract = 6;
///H flag
static const char kGBSFHalfCarry = 5;
///Z flag
static const char kGBSFZero = 7;
///Halt flag
static const char kGBSFHalt = 0;

//basic representation of a opcode
typedef struct command
{
    char* name; //mnemonic
    char numparams;
    void* action; //operation
    uchar cycles;
} command_t;

//The Z80 GameBoy CPU
//has 8 GP 8bit registers which can be combined to 16 bit registers
//(AF, BC, DE, HL)
typedef struct registers
{
    union
    {
        struct {
#ifdef LITTLE_ENDIAN_BYTE_ORDER
            uchar F;
            uchar A;
#else
            
            uchar A;
            uchar F;
#endif
        };
        
        ushort AF;
    };
    
    union
    {
        struct {
#ifdef LITTLE_ENDIAN_BYTE_ORDER
            uchar C;
            uchar B;
#else
            
            uchar B;
            uchar C;
#endif
        };
        
        ushort BC;
    };
    
    union
    {
        struct {
#ifdef LITTLE_ENDIAN_BYTE_ORDER
            uchar E;
            uchar D;
#else
            
            uchar D;
            uchar E;
#endif
        };
        
        ushort DE;
    };
    
    union
    {
        struct {
#ifdef LITTLE_ENDIAN_BYTE_ORDER
            uchar L;
            uchar H;
#else
            
            uchar H;
            uchar L;
#endif
        };
        
        ushort HL;
    };
    
    uchar IE;
    uchar IF;
    
    ushort SP; //stack pointer
    ushort PC; //program counter
    
} registers_t;

typedef struct cpu
{
    uchar ram[8*1024]; //Main RAM
    uchar sram[8*1024];
    uchar oam[256];
    uchar io[0x100];
    uchar hram[128];
    uchar vram[8*1024];
    uchar rom[32*1024];
    char romTitle[17];
    uchar stop;
    ///Number of cycles of the last executed opcode
    unsigned int cycles;
    registers_t regs;
} cpu_t;

int gb_cpu_init();
int gb_load_cartridge(const char* cartridge);
void gb_cpu_step();
void gb_cpu_free();

void gb_set_sf(uchar flag);
void gb_unset_sf(uchar flag);
uchar gb_read_sf(uchar flag);

/*void gb_set_ie(uchar flag);
void gb_unset_ie(uchar flag);
uchar gb_read_ie(uchar flag);

void gb_set_if(uchar flag);
void gb_unset_if(uchar flag);
uchar gb_read_if(uchar flag);*/

int gb_should_stop();
void gb_print_regs();

unsigned long gb_get_cycles();
void gb_reset_cycles();

char* gb_get_rom_title();

int gb_get_step_count();
void gb_reset_step_count();

void gb_toggle_debug();

//actions
void nop(void);
void ld_bc_xx(ushort operand);
void ld_bcp_a();
void inc_bc();
void inc_b();
void dec_b();
void ld_b_x(uchar operand);
void rlca();
void ld_xxp_sp(ushort operand);
void add_hl_bc();
void ld_a_bcp();
void dec_bc();
void inc_c();
void dec_c();
void ld_c_x(uchar operand);
void rrca();
//0x1
void stop();
void ld_de_xx(ushort operand);
void ld_dep_a();
void inc_de();
void inc_d();
void dec_d();
void ld_d_x(uchar operand);
void rla();
void jr_x(uchar operand);
void add_hl_de();
void ld_a_dep();
void dec_de();
void inc_e();
void dec_e();
void ld_e_x(uchar operand);
void rra();
//0x2
void jrnz_x(uchar operand);
void ld_hl_xx(ushort opernad);
void ldi_hlp_a();
void inc_hl();
void inc_h();
void dec_h();
void ld_h_x(uchar opernad);
void daa();
void jrz_x(uchar operand);
void add_hl_hl();
void ldi_a_hlp();
void dec_hl();
void inc_l();
void dec_l();
void ld_l_x(uchar operand);
void cpl();
//0x3
void jrnc_x(uchar operand);
void ld_sp_xx(ushort operand);
void ldd_hlp_a();
void inc_sp();
void inc_hlp();
void dec_hlp();
void ld_hlp_x(uchar operand);
void scf();
void jr_c_x(uchar operand);
void add_hl_sp();
void ldd_a_hlp();
void dec_sp();
void inc_a();
void dec_a();
void ld_a_x(uchar operand);
void ccf();
//0x4
void ld_b_b();
void ld_b_c();
void ld_b_d();
void ld_b_e();
void ld_b_h();
void ld_b_l();
void ld_b_hlp();
void ld_b_a();
void ld_c_b();
void ld_c_c();
void ld_c_d();
void ld_c_e();
void ld_c_h();
void ld_c_l();
void ld_c_hlp();
void ld_c_a();
//0x5
void ld_d_b();
void ld_d_c();
void ld_d_d();
void ld_d_e();
void ld_d_h();
void ld_d_l();
void ld_d_hlp();
void ld_d_a();
void ld_e_b();
void ld_e_c();
void ld_e_d();
void ld_e_e();
void ld_e_h();
void ld_e_l();
void ld_e_hlp();
void ld_e_a();
//0x6
void ld_h_b();
void ld_h_c();
void ld_h_d();
void ld_h_e();
void ld_h_h();
void ld_h_l();
void ld_h_hlp();
void ld_h_a();
void ld_l_b();
void ld_l_c();
void ld_l_d();
void ld_l_e();
void ld_l_h();
void ld_l_l();
void ld_l_hlp();
void ld_l_a();
//0x7
void ld_hlp_b();
void ld_hlp_c();
void ld_hlp_d();
void ld_hlp_e();
void ld_hlp_h();
void ld_hlp_l();
void halt();
void ld_hlp_a();
void ld_a_b();
void ld_a_c();
void ld_a_d();
void ld_a_e();
void ld_a_h();
void ld_a_l();
void ld_a_hlp();
void ld_a_a();
//0x8
void add_a_b();
void add_a_c();
void add_a_d();
void add_a_e();
void add_a_h();
void add_a_l();
void add_a_hlp();
void add_a_a();
void adc_a_b();
void adc_a_c();
void adc_a_d();
void adc_a_e();
void adc_a_h();
void adc_a_l();
void adc_a_hlp();
void adc_a_a();
//0x9
void sub_a_b();
void sub_a_c();
void sub_a_d();
void sub_a_e();
void sub_a_h();
void sub_a_l();
void sub_a_hlp();
void sub_a_a();
void sbc_a_b();
void sbc_a_c();
void sbc_a_d();
void sbc_a_e();
void sbc_a_h();
void sbc_a_l();
void sbc_a_hlp();
void sbc_a_a();
//0xA
void and_b();
void and_c();
void and_d();
void and_e();
void and_h();
void and_l();
void and_hlp();
void and_a();
void xor_b();
void xor_c();
void xor_d();
void xor_e();
void xor_h();
void xor_l();
void xor_hlp();
void xor_a();
//0xB
void or_b();
void or_c();
void or_d();
void or_e();
void or_h();
void or_l();
void or_hlp();
void or_a();
void cp_b();
void cp_c();
void cp_d();
void cp_e();
void cp_h();
void cp_l();
void cp_hlp();
void cp_a();
//0xC
void ret_nz();
void pop_bc();
void jp_nz_xx(ushort operand);
void jp_xx(ushort operand);
void call_nz_xx(ushort operand);
void push_bc();
void add_a_x(uchar operand);
void rst_0();
void ret_z();
void ret();
void jp_z_xx(ushort operand);
void ext_ops(uchar value);
void call_z_xx(ushort operand);
void call_xx(ushort operand);
void adc_a_x(uchar operand);
void rst_8();
//0xD
void ret_nc();
void pop_de();
void jp_nc_xx(ushort adress);
//NOP
void call_nc_xx(ushort adress);
void push_de();
void sub_a_x(uchar operand);
void rst_10();
void ret_c();
void reti();
void jp_c_xx(ushort adress);
//NOP
void call_c_xx(ushort adress);
//NOP
void sbc_a_x(uchar operand);
void rst_18();
//0xE
void ldh_xp_a(uchar operand);
void pop_hl();
//NOP
//NOP
void ldh_cp_a();
void push_hl();
void and_x(uchar operand);
void rst_20();
void add_sp_d();
void jp_hl();
void ld_xxp_a(ushort operand);
//NOP
//NOP
void xor_x(uchar operand);
void rst_28();
//0xF
void ldh_a_xp(uchar operand);
void pop_af();
//NOP
void di();
//NOP
void push_af();
void or_x(uchar operand);
void rst_30();
void ldhl_sp_x(uchar operand);
void ld_sp_hl();
void ld_a_xxp(ushort operand);
void ei();
//NOP
//NOP
void cp_x(uchar operand);
void rst_38();
#endif /* cpu_h */
