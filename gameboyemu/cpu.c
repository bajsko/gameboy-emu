//
//  cpu.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

//
// Error lies on reg 0xff00 - joypad info. Fix it!

#include <stdlib.h>
#include <assert.h>

#include "cpu.h"
#include "gpu.h"
#include "interrupts.h"
#include "memory.h"
#include "cartridge.h"
#include "gbutil.h"
#include "extended_instructions.h"
#include "timers.h"


static cpu_t* cpu = NULL;

static int isCreated() { return cpu != NULL; }
static int stepcount = 0;
static int debug = 0;

static const command_t commandTable[256] =
{
    {"nop", 0, nop, 4}, //0x00
    {"ld bc, xx", 2, ld_bc_xx, 12}, //0x01
    {"ld [bc], a", 0, ld_bcp_a, 8}, //0x02
    {"inc bc", 0, inc_bc, 8}, //0x03
    {"inc b", 0, inc_b, 4}, //0x04
    {"dec b", 0, dec_b, 4}, //0x05
    {"ld b, x", 1, ld_b_x, 8}, //0x06
    {"rlca", 0, rlca, 4}, //0x07
    {"ld (xx), sp'", 2, ld_xxp_sp, 20}, //0x08
    {"add hl, bc", 0, add_hl_bc, 8}, //0x09
    {"ld, a, [bc]", 0, ld_a_bcp, 8}, //0x0A
    {"dec bc", 0, dec_bc, 8}, //0x0B
    {"inc c", 0, inc_c, 4}, //0x0C
    {"dec c", 0, dec_c, 4}, //0x0D
    {"ld c, x", 1, ld_c_x, 8}, //0x0E
    {"rrca", 0, rrca, 4}, //0x0F
    {"stop, x", 1, stop, 4}, //0x10
    {"ld de, xx", 2, ld_de_xx, 12 }, //0x11
    {"ld (de), a", 0, ld_dep_a, 8 }, //0x12
    {"inc de", 0, inc_de, 8}, //0x13
    {"inc d", 0, inc_d, 4}, //0x14
    {"dec d", 0, dec_d, 4}, //0x15
    {"ld d, x", 1, ld_d_x, 8 }, //0x16
    {"rla", 0, rla, 4}, //0x17
    {"jr x", 1, jr_x, 8}, //0x18
    {"add hl, de", 0, add_hl_de, 8}, //0x19
    {"ld a, (de)", 0, ld_a_dep, 8 }, //0x1A
    {"dec de", 0, dec_de, 8}, //0x1B
    {"inc e", 0, inc_e, 4}, //0x1C
    {"dec e", 0, dec_e, 4}, //0x1D
    {"ld e, x", 1, ld_e_x, 8 }, //0x1E
    {"rra", 0, rra, 4}, //0x1F
    {"jrnz, x", 1, jrnz_x, 8}, //0x20
    {"ld hl, xx", 2, ld_hl_xx, 12}, //0x21
    {"ldi (hl) a, hl", 0, ldi_hlp_a, 8 }, //0x22
    {"inc hl", 0, inc_hl, 8}, //0x23
    {"inc h", 0, inc_h, 4}, //0x24
    {"dec h", 0, dec_h, 4}, //0x25
    {"ld h, x", 1, ld_h_x, 8 }, //0x26
    {"daa", 0, daa, 4}, //0x27
    {"jr z, x", 1, jrz_x, 8}, //0x28
    {"add hl, hl", 0, add_hl_hl, 8}, //0x29
    {"ldi a, (hl)", 0, ldi_a_hlp, 8 }, //0x2A
    {"dec hl", 0, dec_hl, 8}, //0x2B
    {"inc l", 0, inc_l, 4}, //0x2C
    {"dec l", 0, dec_l, 4}, //0x2D
    {"ld l, x", 1, ld_l_x, 8 }, //0x2E
    {"cpl", 0, cpl, 4}, //0x2F
    {"jrnc, x", 1, jrnc_x, 8}, //0x30
    {"ld sp, xx", 2, ld_sp_xx, 12}, //0x31
    {"ldd (hl), a", 0, ldd_hlp_a, 8 }, //0x32
    {"inc sp", 0, inc_sp, 8}, //0x33
    {"inc (hl)", 0, inc_hlp, 12}, //0x34
    {"dec (hl)", 0, dec_hlp, 12}, //0x35
    {"ld (hl), x", 1, ld_hlp_x, 12 }, //0x36
    {"scf", 0, scf, 4}, //0x37
    {"jr c, x", 1, jr_c_x, 8}, //0x38
    {"add hl, sp", 0, add_hl_sp, 8}, //0x39
    {"ldd a, (hl)", 0, ldd_a_hlp, 8 }, //0x3A
    {"dec sp", 0, dec_sp, 8}, //0x3B
    {"inc a", 0, inc_a, 4 }, //0x3C
    {"dec a", 0, dec_a, 4}, //0x3D
    {"ld a, x", 1, ld_a_x, 8 }, //0x3E
    {"ccf", 0, ccf, 4}, //0x3F
    {"ld b, b", 0, ld_b_b, 4 }, //0x40
    {"ld b, c", 0, ld_b_c, 4 }, //0x41
    {"ld b, d", 0, ld_b_d, 4 }, //0x42
    {"ld b, e", 0, ld_b_e, 4 }, //0x43
    {"ld b, h", 0, ld_b_h, 4 }, //0x44
    {"ld b, l", 0, ld_b_l, 4 }, //0x45
    {"ld b, (hl)", 0, ld_b_hlp, 8 }, //0x46
    {"ld b, a", 0, ld_b_a, 4 }, //0x47
    {"ld c, b", 0, ld_c_b, 4 }, //0x48
    {"ld c, c", 0, ld_c_c, 4 }, //0x49
    {"ld c, d", 0, ld_c_d, 4 }, //0x4A
    {"ld c, e", 0, ld_c_e, 4}, //0x4B
    {"ld c, h", 0, ld_c_h, 4 }, //0x4C
    {"ld c, l", 0, ld_c_l, 4 }, //0x4D
    {"ld c, (hl)", 0, ld_c_hlp, 8 }, //0x4E
    {"ld c, a", 0, ld_c_a, 4 }, //0x4F
    {"ld d, b", 0, ld_d_b, 4 }, //0x50
    {"ld d, c", 0, ld_d_c, 4 }, //0x51
    {"ld d, d", 0, ld_d_d, 4 }, //0x52
    {"ld d, e", 0, ld_d_e, 4 }, //0x53
    {"ld d, h", 0, ld_d_h, 4 }, //0x54
    {"ld d, l", 0, ld_d_l, 4 }, //0x55
    {"ld d, (hl)", 0, ld_d_hlp, 8}, //0x56
    {"ld d, a", 0, ld_d_a, 4 }, //0x57
    {"ld e, b", 0, ld_e_b, 4 }, //0x58
    {"ld e, c", 0, ld_e_c, 4 }, //0x59
    {"ld e, d", 0, ld_e_d, 4 }, //0x5A
    {"ld e, e", 0, ld_e_e, 4 }, //0x5B
    {"ld e, h", 0, ld_e_h, 4 }, //0x5C
    {"ld e, l", 0, ld_e_l, 4 }, //0x5D
    {"ld e, (hl)", 0, ld_e_hlp, 8 }, //0x5E
    {"ld e, a", 0, ld_e_a, 4 }, //0x5F
    {"ld h, b", 0, ld_h_b, 4 }, //0x60
    {"ld h, c", 0, ld_h_c, 4 }, //0x61
    {"ld h, d", 0, ld_h_d, 4 }, //0x62
    {"ld h, e", 0, ld_h_e, 4 }, //0x63
    {"ld h, h", 0, ld_h_l, 4 }, //0x64
    {"ld h, l", 0, ld_h_l, 4 }, //0x65
    {"ld h, (hl)", 0, ld_h_hlp, 8 }, //0x66
    {"ld h, a", 0, ld_h_a, 4 }, //0x67
    {"ld l, b", 0, ld_l_b, 4 }, //0x68
    {"ld l, c", 0, ld_l_c, 4 }, //0x69
    {"ld l, d", 0, ld_l_d, 4 }, //0x6A
    {"ld l, e", 0, ld_l_e,4 }, //0x6B
    {"ld l, h", 0, ld_l_h, 4 }, //0x6C
    {"ld l, l", 0, ld_l_l, 0 }, //0x6D
    {"ld l, (hl)", 0, ld_l_hlp, 8 }, //0x6E
    {"ld l, a", 0, ld_l_a , 4}, //0x6F
    {"ld (hl), b", 0, ld_hlp_b, 8 }, //0x70
    {"ld (hl), c", 0, ld_hlp_c, 8 }, //0x71
    {"ld (hl), d", 0, ld_hlp_d, 8 }, //0x72
    {"ld (hl), e", 0, ld_hlp_e, 8 }, //0x73
    {"ld (hl), h", 0, ld_hlp_h, 8 }, //0x74
    {"ld (hl), l", 0, ld_hlp_l, 8 }, //0x75
    {"halt", 0, halt, 4}, //0x76
    {"ld (hl), a", 0, ld_hlp_a, 8 }, //0x77
    {"ld a, b", 0, ld_a_b, 4 }, //0x78
    {"ld a, c", 0, ld_a_c, 4 }, //0x79
    {"ld a, d", 0, ld_a_d, 4 }, //0x7A
    {"ld a, e", 0, ld_a_e, 4 }, //0x7B
    {"ld a, h", 0, ld_a_h, 4 }, //0x7C
    {"ld a, l", 0, ld_a_l, 4 }, //0x7D
    {"ld a, (hl)", 0, ld_a_hlp, 8 }, //0x7E
    {"ld a, a", 0, ld_a_a, 4 }, //0x7F
    {"add a, b", 0, add_a_b, 4}, //0x80
    {"add a, c", 0, add_a_c, 4}, //0x81
    {"add a, d", 0, add_a_d, 4}, //0x82
    {"add a, e", 0, add_a_e, 4}, //0x83
    {"add a, h", 0, add_a_h, 4}, //0x84
    {"add a, l", 0, add_a_l, 4}, //0x85
    {"add a, (hl)", 0, add_a_hlp, 8 }, //0x86
    {"add a, a", 0, add_a_a, 4 }, //0x87
    {"adc a, b", 0, adc_a_b, 4 }, //0x88
    {"adc a, c", 0, adc_a_c, 4 }, //0x89
    {"adc a, d", 0, adc_a_d, 4 }, //0x8A
    {"adc a, e", 0, adc_a_e, 4 }, //0x8B
    {"adc a, h", 0, adc_a_h, 4 }, //0x8C
    {"adc a, l", 0, adc_a_l, 4 }, //0x8D
    {"adc a (hl)", 0, adc_a_hlp, 8 }, //0x8E
    {"adc a, a", 0, adc_a_a, 4 }, //0x8F
    {"sub a, b", 0, sub_a_b, 4 }, //0x90
    {"sub a, c", 0, sub_a_c, 4 }, //0x91
    {"sub a, d", 0, sub_a_d, 4 }, //0x92
    {"sub a, e", 0, sub_a_e, 4 }, //0x93
    {"sub a, h", 0, sub_a_h, 4}, //0x94
    {"sub a, l", 0, sub_a_l, 4}, //0x95
    {"sub a, (hl)", 0, sub_a_hlp, 8}, //0x96
    {"sub a, a", 0, sub_a_a, 4}, //0x97
    {"sbc a, b", 0, sbc_a_b, 4}, //0x98
    {"sbc a, c", 0, sbc_a_c, 4}, //0x99
    {"sbc a, d", 0, sbc_a_d, 4}, //0x9A
    {"sbc a, e", 0, sbc_a_e, 4}, //0x9B
    {"sbc a, h", 0, sbc_a_h, 4}, //0x9C
    {"sbc a, l", 0, sbc_a_l, 4}, //0x9D
    {"sbc a, (hl)", 0, sbc_a_hlp, 8}, //0x9E
    {"sbc a, a", 0, sbc_a_a, 4}, //0x9F
    {"and b", 0, and_b, 4}, //0xA0
    {"and c", 0, and_c, 4}, //0xA1
    {"and d", 0, and_d, 4}, //0xA2
    {"and e", 0, and_e, 4}, //0xA3
    {"ane h", 0, and_h, 4}, //0xA4
    {"and l", 0, and_l, 4}, //0xA5
    {"and (hl)", 0, and_hlp, 8}, //0xA6
    {"and a", 0, and_a, 4}, //0xA7
    {"xor b", 0, xor_b, 4}, //0xA8
    {"xor c", 0, xor_c, 4}, //0xA9
    {"xor d", 0, xor_d, 4}, //0xAA
    {"xor e", 0, xor_e, 4}, //0xAB
    {"xor h", 0, xor_h, 4}, //0xAC
    {"xor l", 0, xor_l, 4}, //0xAD
    {"xor (hl)", 0, xor_hlp, 8}, //0xAE
    {"xor a", 0, xor_a, 4}, //0xAF
    {"or b", 0, or_b, 4}, //0xB0
    {"or c", 0, or_c, 4}, //0xB1
    {"or d", 0, or_d, 4}, //0xB2
    {"or e", 0, or_e, 4}, //0xB3
    {"or h", 0, or_h, 4}, //0xB4
    {"or l", 0, or_l, 4}, //0xB5
    {"or (hl)", 0, or_hlp, 8}, //0xB6
    {"or a", 0, or_a, 4}, //0xB7
    {"cp b", 0, cp_b, 4}, //0xB8
    {"cp c", 0, cp_c, 4}, //0xB9
    {"cp d", 0, cp_d, 4}, //0xBA
    {"cp e", 0, cp_e, 4}, //0xBB
    {"cp h", 0, cp_h, 4}, //0xBC
    {"cp l", 0, cp_l, 4}, //0xBD
    {"cp (hl)", 0, cp_hlp, 8}, //0xBE
    {"cp a", 0, cp_a, 4}, //0xBF
    {"ret nz", 0, ret_nz, 8 }, //0xC0
    {"pop bc", 0, pop_bc, 12}, //0xC1
    {"jp nz, xx", 2, jp_nz_xx, 12}, //0xC2
    {"jp, xx", 2, jp_xx, 12}, //0xC3
    {"call nz, xx", 2, call_nz_xx, 12}, //0xC4
    {"push bc", 0, push_bc, 16}, //0xC5
    {"add a, x", 1, add_a_x, 8}, //0xC6
    {"rst00h", 0, rst_0, 32 }, //0xC7
    {"ret z", 0, ret_z, 8 }, //0xC8
    {"ret", 0, ret, 8 }, //0xC9
    {"jp z, xx", 2, jp_z_xx, 12}, //0xCA
    {"BITS", 1, ext_ops, 0}, //0xCB
    {"call z, xx", 2, call_z_xx, 12}, //0xCC
    {"call xx", 2, call_xx, 12}, //0xCD
    {"adc a, x", 1, adc_a_x, 8}, //0xCE
    {"rst08h", 0, rst_8, 32 }, //0xCF
    {"ret nc", 0, ret_nc, 8 }, //0xD0
    {"pop de", 0, pop_de, 12}, //0xD1
    {"jpnc, xx", 2, jp_nc_xx, 12}, //0xD2
    {"out (x), a", 0, nop, 4}, //0xD3
    {"call nc, (xx)", 2, call_nc_xx, 12}, //0xD4
    {"push de", 0, push_de, 16}, //0xD5
    {"sub a, x", 1, sub_a_x, 8}, //0xD6
    {"rst10h", 0, rst_10, 32 }, //0xD7
    {"ret c", 0, ret_c, 8 }, //0xD8
    {"reti", 0, reti, 8 }, //0xD9
    {"jp c, xx", 2, jp_c_xx, 12}, //0xDA
    {"in a, (xx)", 0, nop, 4}, //0xDB
    {"callc, xx", 2, call_c_xx, 12}, //0xDC
    {"IX", 0, nop, 4 }, //0xDD
    {"sbca, x", 1, sbc_a_x, 4 }, //0xDE
    {"rst18h", 0, rst_18, 32 }, //0xDF
    {"ldh, (x), a", 1, ldh_xp_a, 12 }, //0xE0
    {"pop hl", 0, pop_hl, 12}, //0xE1
    {"ldh, cp, a", 0, ldh_cp_a, 8 }, //0xE2
    {"ex (sp), hl", 0, nop, 4}, //0xE3
    {"call po, xx", 0, nop, 4}, //0xE4
    {"push hl", 0, push_hl, 16}, //0xE5
    {"and x", 1, and_x, 8 }, //0xE6
    {"rst20h", 0, rst_20, 32 }, //0xE7
    {"add sp, d", 0, add_sp_d, 16}, //0xE8
    {"jp hl", 0, jp_hl, 4}, //0xE9
    {"ld (xx), a", 2, ld_xxp_a, 16 }, //0xEA
    {"ex de, hl", 0, nop, 4 }, //0xEB
    {"call pe, xx", 0, nop, 4}, //0xEC
    {"EXTD", 0, nop, 4}, //0xED
    {"xor x", 1, xor_x, 8}, //0xEE
    {"rst28h", 0, rst_28, 32 }, //0xEF
    {"ldh a, (x)", 1,  ldh_a_xp, 12}, //0xF0
    {"pop af", 0, pop_af, 12}, //0xF1
    {"jp p, xx", 2, nop, 4 }, //0xF2
    {"di", 0, di, 4}, //0xF3
    {"call p, xx", 0, nop, 4}, //0xF4
    {"push af", 0, push_af, 16 }, //0xF5
    {"or x", 1, or_x, 8 }, //0xF6
    {"rst30h", 0, rst_30, 32 }, //0xF7
    {"ld hl, sp + x", 1, ldhl_sp_x, 12}, //0xF8
    {"ld sp, hl", 0, ld_sp_hl, 8}, //0xF9
    {"ld a, (xx)", 2, ld_a_xxp, 16}, //0xFA
    {"ei", 0, ei, 4}, //0xFB
    {"call m, xx", 0, nop, 4 }, //0xFC
    {"IY", 0, nop, 4}, //0xFD
    {"cp x", 1, cp_x, 8}, //0xFE
    {"rst38h", 0, rst_38, 32 }, //0xFF
    
};

int gb_cpu_init()
{
    if(isCreated())
        return GB_ERROR_CPU_ALREADY_CREATED;
    
    cpu_t* l_cpu = (cpu_t*)malloc(sizeof(cpu_t));
    if(!l_cpu)
    {
        return GB_ERROR_MEMORY;
    }
    
    memset(l_cpu->rom, 0, sizeof(l_cpu->rom)); //32kB of 0's
    memset(l_cpu->ram, 0, sizeof(l_cpu->ram));
    memset(l_cpu->vram, 0, sizeof(l_cpu->vram));
    memset(l_cpu->romTitle, 0, sizeof(l_cpu->romTitle));
    memset(l_cpu->io, 0, sizeof(l_cpu->io));
    memset(l_cpu->hram, 0, sizeof(l_cpu->hram));
    memset(l_cpu->oam, 0, sizeof(l_cpu->oam));
    
    gb_log("Initiated CPU!");
    gb_log("Working RAM: %dkB", sizeof(l_cpu->rom)/1024);
    gb_log("Video RAM: %dkB", sizeof(l_cpu->ram)/1024);
    gb_log("ROM: %dkB", sizeof(l_cpu->vram)/1024);
    
    cpu = l_cpu;
    
    gb_memory_attach(cpu);
    gb_interrupt_attach(cpu);
    gb_timers_attach(cpu);
    gb_gpu_attach(cpu);
    gb_extended_instructions_attach(cpu);
    
    cpu->cycles = 0;
    
    cpu->regs.A = 0x01;
    cpu->regs.F = 0xb0;
    cpu->regs.B = 0x00;
    cpu->regs.C = 0x13;
    cpu->regs.D = 0x00;
    cpu->regs.E = 0xd8;
    cpu->regs.H = 0x01;
    cpu->regs.L = 0x4d;
    cpu->regs.SP = 0xfffe;
    cpu->regs.PC = 0x100;
    
    writeByte(0xFF05, 0);
    writeByte(0xFF06, 0);
    writeByte(0xFF07, 0);
    writeByte(0xFF10, 0x80);
    writeByte(0xFF11, 0xBF);
    writeByte(0xFF12, 0xF3);
    writeByte(0xFF14, 0xBF);
    writeByte(0xFF16, 0x3F);
    writeByte(0xFF17, 0x00);
    writeByte(0xFF19, 0xBF);
    writeByte(0xFF1A, 0x7A);
    writeByte(0xFF1B, 0xFF);
    writeByte(0xFF1C, 0x9F);
    writeByte(0xFF1E, 0xBF);
    writeByte(0xFF20, 0xFF);
    writeByte(0xFF21, 0x00);
    writeByte(0xFF22, 0x00);
    writeByte(0xFF23, 0xBF);
    writeByte(0xFF24, 0x77);
    writeByte(0xFF25, 0xF3);
    writeByte(0xFF26, 0xF1);
    writeByte(0xFF40, 0x91);
    writeByte(0xFF42, 0x00);
    writeByte(0xFF43, 0x00);
    writeByte(0xFF45, 0x00);
    writeByte(0xFF47, 0xFC);
    writeByte(0xFF48, 0xFF);
    writeByte(0xFF49, 0xFF);
    writeByte(0xFF4A, 0x00);
    writeByte(0xFF4B, 0x00);
    writeByte(0xFFFF, 0x00);
    
    writeByte(0xff00, 0xff);
    
    gb_interrupt_set_master(1);
    
    return 0;
}

int gb_load_cartridge(const char* cartridgePath)
{
    assert(cpu != NULL || cartridgePath != NULL);
    
    if(!isCreated())
        return GB_ERROR_NO_CPU;
    
    gb_stopwatch_start();
    
    unsigned long fsize = 0;
    readFileToBuffer(cpu->rom, cartridgePath, &fsize);
    
    if(gb_getIOError() != 0)
    {
        gb_log("IO error occourd during loading of cartridge, error code: %d", gb_getIOError());
        return -1;
    }
    
    memcpy(cpu->romTitle, cpu->rom+kGBCartridgeTitleOffset, sizeof(cpu->romTitle));
    cpu->romTitle[16] = '\0';
    
    uchar romType = readByte(kGBCartridgeTypeOffset);
    uchar deviceType = readByte(kGBDeviceTypeOffset);
    
    float loadTime = gb_stopwatch_end();
    
    gb_log("Loaded cartridge '%s' (type 0x%x, device type 0x%x) (took %f seconds)", cpu->romTitle, romType, deviceType, loadTime);
    
    return 0;
}

void gb_cpu_step()
{
    if(gb_read_sf(kGBSFHalt))
    {
        return;
    }
    
    stepcount++;
    
    /*if(cpu->regs.PC == 0x282a)
    {
        //gb_set_sf(kGBSFHalt);
        
        gb_print_regs();
    }*/
    
    if(cpu->regs.PC == 0x38a) {
        /*FILE *f = fopen("tileset1.txt", "wb");
        for(int i = 0; i < 128; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                uchar b1 = cpu->vram[(((8*i)+j)*2)];
                uchar b2 = cpu->vram[((((8*i)+j)*2)+1)];
            
                for(int k = 0; k < 8; k++)
                {
                    char bit = (k-7)*-1;
                    uchar value = (fetchBit(&b1, bit) << 1) + fetchBit(&b2, bit);
                    fprintf(f, "%02x", value);
                }
            
                fprintf(f, "\n");
            }
            
            fprintf(f, "\n");
        }
        fclose(f);
        
        gb_print_regs();*/
        //gb_set_sf(kGBSFHalt);
    }
    
    uchar opcode = readByte(cpu->regs.PC);
    
    command_t command = commandTable[opcode];
    if(commandTable[opcode].action == NULL)
    {
        gb_log("Unimplemented instruction found!\n--> opcode: 0x%02x\n--> mnemonic: %s\n--> pc: 0x%02x",
               opcode, command.name, cpu->regs.PC);
        gb_set_sf(kGBSFHalt);
        gb_log("Halt flag set!");
        
        gb_print_regs();
        
        return;
    }
    
#ifdef DEBUG
    if(debug)
        gb_log("%s (0x%02x) (PC: 0x%2X)", command.name, opcode, cpu->regs.PC);
#endif
    
    cpu->regs.PC++;
    
    ushort operand = 0;
    if(command.numparams == 1) operand = (ushort)readByte(cpu->regs.PC);
    if(command.numparams == 2) operand = readShort(cpu->regs.PC);
    
    cpu->regs.PC += command.numparams;
    cpu->cycles = command.cycles;
    
    switch (command.numparams) {
        case 0:
            ((void(*)(void))command.action)();
            break;
            
        case 1:
            ((void(*)(uchar))command.action)((uchar)operand);
            break;
            
        case 2:
            ((void(*)(ushort))command.action)((ushort)operand);
            break;
            
        default:
            break;
    }
    
    if(cpu->cycles == 0)
        assert(0);
}
void gb_cpu_free()
{
    if(!isCreated())
        return;
    
    free(cpu);
    cpu = NULL;
    
    gb_memory_detach();
    gb_timers_detach();
    gb_gpu_detach();
    gb_interrupt_detach();
    gb_extended_instructions_detach();
    gb_log("Freed CPU");
}

void gb_set_sf(uchar flag)
{
    assert(isCreated());
    
    cpu->regs.F |= (1 << flag);
}

void gb_unset_sf(uchar flag)
{
    assert(isCreated());
    
    uchar c = ~cpu->regs.F;
    c |= (1 << flag);
    cpu->regs.F = ~c;
}

uchar gb_read_sf(uchar flag)
{
    assert(isCreated());
    
    uchar c = cpu->regs.F & (1 << flag);
    c >>= flag;
    return c;
}

int gb_should_stop()
{
    assert(cpu != NULL);
    return cpu->stop;
}

void gb_print_regs()
{
    uchar a, b, c, d, e, h, l, f;
    a = cpu->regs.A;
    f = cpu->regs.F;
    b = cpu->regs.B;
    c = cpu->regs.C;
    d = cpu->regs.D;
    e = cpu->regs.E;
    h = cpu->regs.H;
    l = cpu->regs.L;
    
    gb_log("========================");
    gb_log("      Registers");
    gb_log("A: 0x%02x", a);
    gb_log("F: 0x%02x", f);
    gb_log("B: 0x%02x", b);
    gb_log("C: 0x%02x", c);
    gb_log("D: 0x%02x", d);
    gb_log("E: 0x%02x", e);
    gb_log("H: 0x%02x", h);
    gb_log("L: 0x%02x", l);
    gb_log("   16-bit Registers:");
    gb_log("SP: 0x%02x", cpu->regs.SP);
    gb_log("PC: 0x%02x", cpu->regs.PC);
    gb_log("IF: 0x%02x", gb_interrupt_get_if());
    gb_log("IE: 0x%02x", gb_interrupt_get_ie());
    gb_log("========================");
}

///Returns the number of cycles used this step.
unsigned long gb_get_cycles()
{
    assert(cpu != NULL);
    return cpu->cycles;
}

void gb_reset_cycles()
{
    assert(cpu != NULL);
    cpu->cycles = 0;
}

void gb_toggle_debug()
{
    debug ^= 1;
}

char* gb_get_rom_title()
{
    assert(cpu != NULL);
    return cpu->romTitle;
}

int gb_get_step_count()
{
    return stepcount;
}
void gb_reset_step_count()
{
    stepcount = 0;
}

//Opcodes

static uchar incf(uchar c)
{
    if((c & 0xf) == 0xf) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    c++;
    
    if((c & 0xFF) == 0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    
    //sign and parity/overflow flag should not be necessary
    
    return c;
}

static uchar decf(uchar c)
{
    if(c & 0x0f) gb_unset_sf(kGBSFHalfCarry);
    else gb_set_sf(kGBSFHalfCarry);
    
    c--;
    
    if((c) == 0)
        gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_set_sf(kGBSFSubtract);
    
    return c;
}

static void add(uchar *dest, uchar value)
{
    ushort sum = *dest + value;
    
    if((*dest & 0x0f) + (value & 0x0f) > 0xf) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    if(sum & 0xff00) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(sum == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    
    *dest = (uchar)(sum & 0xff);
}

static void adc(uchar* dest, uchar value)
{
    value += gb_read_sf(kGBSFCarry);
    ushort sum = *dest + value;
    
    if(((*dest & 0x0f) + (value & 0x0f)) > 0x0f) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    if(sum & 0xff00) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(sum == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    
    *dest = (uchar)(sum & 0xff);
}

static void sub(uchar* dest, uchar value)
{
    uchar diff = *dest - value;
    
    if((value & 0x0f) > (*dest & 0x0f)) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    if(value > *dest) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(diff == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_set_sf(kGBSFSubtract);
    
    *dest = diff;
}

static void sbc(uchar* dest, uchar value)
{
    value += gb_read_sf(kGBSFCarry);
    uchar diff = *dest - (value);
    
    if((value & 0x0f) > (*dest & 0x0f)) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    if(value > *dest) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(diff == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_set_sf(kGBSFSubtract);
    
    *dest = diff;
}

static void add2(ushort* dest, ushort value)
{
    uint32_t result = *dest + value;
    if(result & 0xffff00000) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    *dest = (ushort)(result & 0xffff);
    
    if(((*dest & 0x0f) + (value & 0x0f)) > 0x0f) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    gb_unset_sf(kGBSFSubtract);
}

static void and(uchar* dest, uchar reg)
{
    uchar res = reg & *dest;
    
    if(res == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_set_sf(kGBSFHalfCarry);
    gb_unset_sf(kGBSFCarry);
    gb_unset_sf(kGBSFSubtract);
    
    *dest = res;
}

static void xor(uchar* dest, uchar reg)
{
    uchar res = reg ^ *dest;
    
    if(res == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFHalfCarry);
    gb_unset_sf(kGBSFCarry);
    gb_unset_sf(kGBSFSubtract);
    
    *dest = res;
}

static void or(uchar* dest, uchar reg)
{
    uchar res = reg | *dest;
    
    if(res == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFHalfCarry);
    gb_unset_sf(kGBSFCarry);
    gb_unset_sf(kGBSFSubtract);
    
    *dest = res;
}

static void cp(uchar reg)
{
    uchar areg = cpu->regs.A;
    
    if((reg & 0x0f) > (areg & 0x0f)) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    if(reg > areg) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(areg == reg) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_set_sf(kGBSFSubtract);
}

static void call(ushort adress)
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = adress;
}

void nop(void) { }

void ld_bc_xx(ushort operand)
{
    cpu->regs.BC = operand;
}

void ld_bcp_a()
{
    uchar a = cpu->regs.A;
    ushort index = cpu->regs.BC;
    writeByte(index, a);
}

void inc_bc()
{
    cpu->regs.BC++;
}

void inc_b()
{
    cpu->regs.B = incf(cpu->regs.B);
}

void dec_b()
{
    cpu->regs.B = decf(cpu->regs.B);
}

void ld_b_x(uchar operand)
{
    cpu->regs.B = operand;
}
void rlca()
{
    uchar a = cpu->regs.A;
    uchar bit7 = fetchBit(&a, 7);
    
    if((bit7 & 0x1) == 0x1) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    a <<= 1;
    a |= bit7;
    
    cpu->regs.A = a;
    
    if(a == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
    
}
void ld_xxp_sp(ushort operand)
{
    ushort index = operand;
    writeShort(index, cpu->regs.SP);
}

void add_hl_bc()
{
    add2(&cpu->regs.HL, cpu->regs.BC);
}
void ld_a_bcp()
{
    ushort index = cpu->regs.BC;
    cpu->regs.A = readByte(index);
}
void dec_bc()
{
    cpu->regs.BC--;
}
void inc_c()
{
    cpu->regs.C = incf(cpu->regs.C);
}
void dec_c()
{
    cpu->regs.C = decf(cpu->regs.C);
}
void ld_c_x(uchar operand)
{
    cpu->regs.C = operand;
}
void rrca()
{
    uchar a = cpu->regs.A;
    uchar bit0 = fetchBit(&a, 0);
    
    if((bit0 & 0x1) == 0x1) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    a >>= 1;
    bit0 <<= 7;
    a |= bit0;
    
    cpu->regs.A = a;
    
    if(a == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
}
//0x1
void stop()
{
    cpu->stop = 1;
}

void ld_de_xx(ushort operand)
{
    cpu->regs.DE = operand;
}

void ld_dep_a()
{
    ushort index = cpu->regs.DE;
    writeByte(index, cpu->regs.A);
}

void inc_de()
{
    cpu->regs.DE++;
}

void inc_d()
{
    cpu->regs.D = incf(cpu->regs.D);
}

void dec_d()
{
    cpu->regs.D = decf(cpu->regs.D);
}

void ld_d_x(uchar operand)
{
    cpu->regs.D = operand;
}

void rla()
{
    uchar areg = cpu->regs.A;
    uchar bit7 = fetchBit(&areg, 7);
    
    areg <<= 1;
    areg |= gb_read_sf(kGBSFCarry);
    
    if(bit7 == 0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    cpu->regs.A = areg;
    
    if(areg == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
}

void jr_x(uchar operand)
{
    cpu->regs.PC += (signed char)operand;
}

void add_hl_de()
{
    add2(&cpu->regs.HL, cpu->regs.DE);
}

void ld_a_dep()
{
    ushort index = cpu->regs.DE;
    cpu->regs.A = readByte(index);
}

void dec_de()
{
    cpu->regs.DE--;
}

void inc_e()
{
    cpu->regs.E = incf(cpu->regs.E);
}

void dec_e()
{
    cpu->regs.E = decf(cpu->regs.E);
}

void ld_e_x(uchar operand)
{
    cpu->regs.E = operand;
}

void rra()
{
    uchar areg = cpu->regs.A;
    uchar bit0 = fetchBit(&areg, 0);
    
    areg >>= 1;
    areg |= (gb_read_sf(kGBSFCarry) << 7);
    
    if(bit0 == 0) gb_unset_sf(kGBSFCarry);
    else gb_set_sf(kGBSFCarry);
    
    cpu->regs.A = areg;
    
    if(areg == 0x0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
}

void jrnz_x(uchar operand)
{
    if(gb_read_sf(kGBSFZero) == 1)
        return;
    
    cpu->regs.PC += (signed char)operand;
}

void ld_hl_xx(ushort operand)
{
    cpu->regs.HL = operand;
}

void ldi_hlp_a()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, cpu->regs.A);
    cpu->regs.HL++;
}

void inc_hl()
{
    cpu->regs.HL++;
}

void inc_h()
{
    cpu->regs.H = incf(cpu->regs.H);
}

void dec_h()
{
    cpu->regs.H = decf(cpu->regs.H);
}

void ld_h_x(uchar operand)
{
    cpu->regs.H = operand;
}

void daa()
{
    uchar a = cpu->regs.A;
    uchar correctionFactor = 0;
    
    if(a > 0x99 || gb_read_sf(kGBSFCarry) == 1)
    {
        correctionFactor = 6;
        correctionFactor <<= 4;
        gb_set_sf(kGBSFCarry);
    } else
    {
        correctionFactor = 0;
        gb_unset_sf(kGBSFCarry);
    }
    
    if((a & 0x0F) > 9 || gb_read_sf(kGBSFHalfCarry) == 1)
        correctionFactor |= 6;
    else
        correctionFactor |= (~0xF);
    
    if(gb_read_sf(kGBSFSubtract) == 0)
        a += correctionFactor;
    else
        a -= correctionFactor;
    
    gb_unset_sf(kGBSFHalfCarry);
    
    if(a == 0) gb_set_sf(kGBSFZero);
    else gb_unset_sf(kGBSFZero);
}

void jrz_x(uchar operand)
{
    if(gb_read_sf(kGBSFZero) == 0)
        return;
    
    cpu->regs.PC += (signed char)operand;
}

void add_hl_hl()
{
    add2(&cpu->regs.HL, cpu->regs.HL);
}

void ldi_a_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.A = readByte(adress);
    cpu->regs.HL++;
}

void dec_hl()
{
    cpu->regs.HL--;
}

void inc_l()
{
    cpu->regs.L = incf(cpu->regs.L);
}

void dec_l()
{
    cpu->regs.L = decf(cpu->regs.L);
}

void ld_l_x(uchar operand)
{
    cpu->regs.L = operand;
}

void cpl()
{
    uchar areg = cpu->regs.A;
    areg = ~areg;
    
    gb_set_sf(kGBSFSubtract);
    gb_set_sf(kGBSFHalfCarry);
    
    cpu->regs.A = areg;
}
//0x3
void jrnc_x(uchar operand)
{
    if(gb_read_sf(kGBSFCarry) == 1)
        return;
    
    cpu->regs.PC += (signed char)operand;
}

void ld_sp_xx(ushort operand)
{
    cpu->regs.SP = operand;
}

void ldd_hlp_a()
{
    ushort adress = cpu->regs.HL;
    writeByte(adress, cpu->regs.A);
    cpu->regs.HL--;
}

void inc_sp()
{
    cpu->regs.SP++;
}

void inc_hlp()
{
    uchar r = readByte(cpu->regs.HL);
    r = incf(r);
    writeByte(cpu->regs.HL, r);
}

void dec_hlp()
{
    uchar r = readByte(cpu->regs.HL);
    r = decf(r);
    writeByte(cpu->regs.HL, r);
}

void ld_hlp_x(uchar operand)
{
    ushort offset = cpu->regs.HL;
    writeByte(offset, operand);
}

void scf()
{
    gb_set_sf(kGBSFCarry);
    gb_unset_sf(kGBSFHalfCarry);
    gb_unset_sf(kGBSFSubtract);
}

void jr_c_x(uchar operand)
{
    if(gb_read_sf(kGBSFCarry) == 0)
        return;
    
    cpu->regs.PC += (signed char)operand;
}

void add_hl_sp()
{
    add2(&cpu->regs.HL, cpu->regs.SP);
}

void ldd_a_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.A = readByte(adress);
    cpu->regs.HL--;
}

void dec_sp()
{
    cpu->regs.SP--;
}

void inc_a()
{
    cpu->regs.A = incf(cpu->regs.A);
}

void dec_a()
{
    cpu->regs.A = decf(cpu->regs.A);
}

void ld_a_x(uchar operand)
{
    cpu->regs.A = operand;
}

void ccf()
{
    if(gb_read_sf(kGBSFCarry) == 1)
        gb_unset_sf(kGBSFCarry);
    else
        gb_set_sf(kGBSFCarry);
    
    gb_unset_sf(kGBSFSubtract);
    gb_unset_sf(kGBSFHalfCarry);
}

void ld_b_b()
{
    nop();
}

void ld_b_c()
{
    cpu->regs.B = cpu->regs.C;
}

void ld_b_d()
{
    cpu->regs.B = cpu->regs.D;
}

void ld_b_e()
{
    cpu->regs.B = cpu->regs.E;
}

void ld_b_h()
{
    cpu->regs.B = cpu->regs.H;
}

void ld_b_l()
{
    cpu->regs.B = cpu->regs.L;
}

void ld_b_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.B = readByte(adress);
}

void ld_b_a()
{
    cpu->regs.B = cpu->regs.A;
}

void ld_c_b()
{
    cpu->regs.C = cpu->regs.B;
}

void ld_c_c()
{
    nop();
}

void ld_c_d()
{
    cpu->regs.C = cpu->regs.D;
}

void ld_c_e()
{
    cpu->regs.C = cpu->regs.E;
}

void ld_c_h()
{
    cpu->regs.C = cpu->regs.H;
}

void ld_c_l()
{
    cpu->regs.C = cpu->regs.L;
}

void ld_c_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.C = readByte(adress);
}

void ld_c_a()
{
    cpu->regs.C = cpu->regs.A;
}

void ld_d_b()
{
    cpu->regs.D = cpu->regs.B;
}

void ld_d_c()
{
    cpu->regs.D = cpu->regs.C;
}

void ld_d_d()
{
    nop();
}

void ld_d_e()
{
    cpu->regs.D = cpu->regs.E;
}

void ld_d_h()
{
    cpu->regs.D = cpu->regs.H;
}

void ld_d_l()
{
    cpu->regs.D = cpu->regs.L;
}

void ld_d_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.D = readByte(adress);
}

void ld_d_a()
{
    cpu->regs.D = cpu->regs.A;
}

void ld_e_b()
{
    cpu->regs.E = cpu->regs.B;
}

void ld_e_c()
{
    cpu->regs.E = cpu->regs.C;
}

void ld_e_d()
{
    cpu->regs.E = cpu->regs.D;
}

void ld_e_e()
{
    nop();
}

void ld_e_h()
{
    cpu->regs.E = cpu->regs.H;
}

void ld_e_l()
{
    cpu->regs.E = cpu->regs.L;
}

void ld_e_hlp()
{
    ushort address = cpu->regs.HL;
    cpu->regs.E = readByte(address);
}

void ld_e_a()
{
    cpu->regs.E = cpu->regs.A;
}

void ld_h_b()
{
    cpu->regs.H = cpu->regs.B;
}

void ld_h_c()
{
    cpu->regs.H = cpu->regs.C;
}

void ld_h_d()
{
    cpu->regs.H = cpu->regs.D;
}

void ld_h_e()
{
    cpu->regs.H = cpu->regs.E;
}

void ld_h_h()
{
    nop();
}

void ld_h_l()
{
    cpu->regs.H = cpu->regs.L;
}

void ld_h_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.H = readByte(adress);
}

void ld_h_a()
{
    cpu->regs.H = cpu->regs.A;
}

void ld_l_b()
{
    cpu->regs.L = cpu->regs.B;
}

void ld_l_c()
{
    cpu->regs.L = cpu->regs.C;
}

void ld_l_d()
{
    cpu->regs.L = cpu->regs.D;
}

void ld_l_e()
{
    cpu->regs.L = cpu->regs.E;
}

void ld_l_h()
{
    cpu->regs.L = cpu->regs.H;
}

void ld_l_l()
{
    nop();
}

void ld_l_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.L = readByte(adress);
}

void ld_l_a()
{
    cpu->regs.L = cpu->regs.A;
}

void ld_hlp_b()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.B;
    writeByte(adress, c);
}

void ld_hlp_c()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.C;
    writeByte(adress, c);
}

void ld_hlp_d()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.D;
    writeByte(adress, c);
}

void ld_hlp_e()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.E;
    writeByte(adress, c);
}

void ld_hlp_h()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.H;
    writeByte(adress, c);
}

void ld_hlp_l()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.L;
    writeByte(adress, c);
}

void halt()
{
    gb_set_sf(kGBSFHalt);
}

void ld_hlp_a()
{
    ushort adress = cpu->regs.HL;
    uchar c = cpu->regs.A;
    writeByte(adress, c);
}

void ld_a_b()
{
    cpu->regs.A = cpu->regs.B;
}

void ld_a_c()
{
    cpu->regs.A = cpu->regs.C;
}

void ld_a_d()
{
    cpu->regs.A = cpu->regs.D;
}

void ld_a_e()
{
    cpu->regs.A = cpu->regs.E;
}

void ld_a_h()
{
    cpu->regs.A = cpu->regs.H;
}

void ld_a_l()
{
    cpu->regs.A = cpu->regs.L;
}

void ld_a_hlp()
{
    ushort adress = cpu->regs.HL;
    cpu->regs.A = readByte(adress);
}

void ld_a_a()
{
    nop();
}

void add_a_b()
{
    add(&cpu->regs.A, cpu->regs.B);
}

void add_a_c()
{
    add(&cpu->regs.A, cpu->regs.C);
}

void add_a_d()
{
    add(&cpu->regs.A, cpu->regs.D);
}

void add_a_e()
{
    add(&cpu->regs.A, cpu->regs.E);
}

void add_a_h()
{
    add(&cpu->regs.A, cpu->regs.H);
}

void add_a_l()
{
    add(&cpu->regs.A, cpu->regs.L);
}

void add_a_hlp()
{
    ushort adress = cpu->regs.HL;
    add(&cpu->regs.A, readByte(adress));
}

void add_a_a()
{
    add(&cpu->regs.A, cpu->regs.A);
}

void adc_a_b()
{
    adc(&cpu->regs.A, cpu->regs.B);
}

void adc_a_c()
{
    adc(&cpu->regs.A, cpu->regs.C);
}

void adc_a_d()
{
    adc(&cpu->regs.A, cpu->regs.D);
}

void adc_a_e()
{
    adc(&cpu->regs.A, cpu->regs.E);
}

void adc_a_h()
{
    adc(&cpu->regs.A, cpu->regs.H);
}

void adc_a_l()
{
    adc(&cpu->regs.A, cpu->regs.L);
}

void adc_a_hlp()
{
    ushort adress = cpu->regs.HL;
    adc(&cpu->regs.A, readByte(adress));
}

void adc_a_a()
{
    adc(&cpu->regs.A, cpu->regs.A);
}

void sub_a_b()
{
    sub(&cpu->regs.A, cpu->regs.B);
}

void sub_a_c()
{
    sub(&cpu->regs.A, cpu->regs.C);
}

void sub_a_d()
{
    sub(&cpu->regs.A, cpu->regs.D);
}

void sub_a_e()
{
    sub(&cpu->regs.A, cpu->regs.E);
}

void sub_a_h()
{
    sub(&cpu->regs.A, cpu->regs.H);
}

void sub_a_l()
{
    sub(&cpu->regs.A, cpu->regs.L);
}

void sub_a_hlp()
{
    ushort adress = cpu->regs.HL;
    sub(&cpu->regs.A, readByte(adress));
}

void sub_a_a()
{
    sub(&cpu->regs.A, cpu->regs.A);
}

void sbc_a_b()
{
    sbc(&cpu->regs.A, cpu->regs.B);
}

void sbc_a_c()
{
    sbc(&cpu->regs.A, cpu->regs.C);
}

void sbc_a_d()
{
    sbc(&cpu->regs.A, cpu->regs.D);
}

void sbc_a_e()
{
    sbc(&cpu->regs.A, cpu->regs.E);
}

void sbc_a_h()
{
    sbc(&cpu->regs.A, cpu->regs.H);
}

void sbc_a_l()
{
    sbc(&cpu->regs.A, cpu->regs.L);
}

void sbc_a_hlp()
{
    ushort adress = cpu->regs.HL;
    sbc(&cpu->regs.A, readByte(adress));
}

void sbc_a_a()
{
    sbc(&cpu->regs.A, cpu->regs.A);
}

void and_b()
{
    and(&cpu->regs.A, cpu->regs.B);
}

void and_c()
{
    and(&cpu->regs.A, cpu->regs.C);
}

void and_d()
{
    and(&cpu->regs.A, cpu->regs.D);
}

void and_e()
{
    and(&cpu->regs.A, cpu->regs.E);
}

void and_h()
{
    and(&cpu->regs.A, cpu->regs.H);
}

void and_l()
{
    and(&cpu->regs.A, cpu->regs.L);
}

void and_hlp()
{
    ushort adress = cpu->regs.HL;
    and(&cpu->regs.A, readByte(adress));
}

void and_a()
{
    and(&cpu->regs.A, cpu->regs.A);
}

void xor_b()
{
    xor(&cpu->regs.A, cpu->regs.B);
}

void xor_c()
{
    xor(&cpu->regs.A, cpu->regs.C);
}

void xor_d()
{
    xor(&cpu->regs.A, cpu->regs.D);
}

void xor_e()
{
    xor(&cpu->regs.A, cpu->regs.E);
}

void xor_h()
{
    xor(&cpu->regs.A, cpu->regs.H);
}

void xor_l()
{
    xor(&cpu->regs.A, cpu->regs.L);
}

void xor_hlp()
{
    ushort adress = cpu->regs.HL;
    xor(&cpu->regs.A, readByte(adress));
}

void xor_a()
{
    xor(&cpu->regs.A, cpu->regs.A);
}

void or_b()
{
    or(&cpu->regs.A, cpu->regs.B);
}

void or_c()
{
    or(&cpu->regs.A, cpu->regs.C);
}

void or_d()
{
    or(&cpu->regs.A, cpu->regs.D);
}

void or_e()
{
    or(&cpu->regs.A, cpu->regs.E);
}

void or_h()
{
    or(&cpu->regs.A, cpu->regs.H);
}

void or_l()
{
    or(&cpu->regs.A, cpu->regs.L);
}

void or_hlp()
{
    ushort adress = cpu->regs.HL;
    or(&cpu->regs.A, readByte(adress));
}

void or_a()
{
    or(&cpu->regs.A, cpu->regs.A);
}

void cp_b()
{
    cp(cpu->regs.B);
}

void cp_c()
{
    cp(cpu->regs.C);
}

void cp_d()
{
    cp(cpu->regs.D);
}

void cp_e()
{
    cp(cpu->regs.E);
}

void cp_h()
{
    cp(cpu->regs.H);
}

void cp_l()
{
    cp(cpu->regs.L);
}

void cp_hlp()
{
    ushort adress = cpu->regs.HL;
    cp(readByte(adress));
}

void cp_a()
{
    cp(cpu->regs.A);
}

void ret_nz()
{
    if(gb_read_sf(kGBSFZero) == 1)
        return;
    
    ret();
}

void pop_bc()
{
    cpu->regs.BC = popShortFromStack();
}

void jp_nz_xx(ushort operand)
{
    if(gb_read_sf(kGBSFZero) == 1)
        return;
    
    cpu->regs.PC = operand;
}

void jp_xx(ushort operand)
{
    cpu->regs.PC = operand;
}

void call_nz_xx(ushort operand)
{
    if(gb_read_sf(kGBSFZero) == 1)
        return;
    
    call(operand);
}

void push_bc()
{
    pushShortToStack(cpu->regs.BC);
}

void add_a_x(uchar operand)
{
    add(&cpu->regs.A, operand);
}

void rst_0()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0000;
}

void ret_z()
{
    if(gb_read_sf(kGBSFZero) == 0)
        return;
    
    ret();
}

void ret()
{
    ushort adress = popShortFromStack();
    cpu->regs.PC = adress;
}

void jp_z_xx(ushort adress)
{
    if(gb_read_sf(kGBSFZero) == 0)
        return;
    
    cpu->regs.PC = adress;
}

void ext_ops(uchar value)
{
    gb_extended(value);
}

void call_z_xx(ushort operand)
{
    if(gb_read_sf(kGBSFZero) == 0)
        return;
    
    call(operand);
}

void call_xx(ushort operand)
{
    call(operand);
}

void adc_a_x(uchar operand)
{
    adc(&cpu->regs.A, operand);
}

void rst_8()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0008;
}

void ret_nc()
{
    if(gb_read_sf(kGBSFCarry) == 1)
        return;
    
    ret();
}

void pop_de()
{
    cpu->regs.DE = popShortFromStack();
}

void jp_nc_xx(ushort adress)
{
    if(gb_read_sf(kGBSFCarry) == 1)
        return;
    
    cpu->regs.PC = adress;
}

void call_nc_xx(ushort adress)
{
    if(gb_read_sf(kGBSFCarry) == 1)
        return;
    
    call(adress);
}

void push_de()
{
    pushShortToStack(cpu->regs.DE);
}

void sub_a_x(uchar operand)
{
    sub(&cpu->regs.A, operand);
}

void rst_10()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0010;
}

void ret_c()
{
    if(gb_read_sf(kGBSFCarry) == 0)
        return;
    
    ret();
}

void reti()
{
    gb_interrupt_set_master(1);
    ret();
}

void jp_c_xx(ushort adress)
{
    if(gb_read_sf(kGBSFCarry) == 0)
        return;
    
    cpu->regs.PC = adress;
}

void call_c_xx(ushort adress)
{
    if(gb_read_sf(kGBSFCarry) == 0)
        return;
    
    call(adress);
}

void sbc_a_x(uchar operand)
{
    sbc(&cpu->regs.A, operand);
}

void rst_18()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0018;
}

void ldh_xp_a(uchar operand)
{
    ushort adress = 0xFF00 + operand;
    //gb_log("Wrote to adress 0x%04x, : %d", adress, cpu->regs.A);
    writeByte(adress, cpu->regs.A);
}

void pop_hl()
{
    cpu->regs.HL = popShortFromStack();
}

void ldh_cp_a()
{
    ushort adress = 0xFF00 + cpu->regs.C;
    writeByte(adress, cpu->regs.A);
}

void push_hl()
{
    pushShortToStack(cpu->regs.HL);
}

void and_x(uchar operand)
{
    and(&cpu->regs.A, operand);
}

void rst_20()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0020;
}

void add_sp_d()
{
    cpu->regs.SP += (signed char)cpu->regs.D;
}

void jp_hl()
{
    cpu->regs.PC = cpu->regs.HL;
}

void ld_xxp_a(ushort operand)
{
    writeByte(operand, cpu->regs.A);
}

void xor_x(uchar operand)
{
    xor(&cpu->regs.A, operand);
}

void rst_28()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0028;
}

void ldh_a_xp(uchar operand)
{
    cpu->regs.A = readByte(0xff00 + operand);
    //gb_log("Read %d from adress 0x%02x", cpu->regs.A, 0xff00 + operand);
}

void pop_af()
{
    cpu->regs.AF = popShortFromStack();
}

void di()
{
    gb_interrupt_set_master(0);
}

void push_af()
{
    pushShortToStack(cpu->regs.AF);
}

void or_x(uchar operand)
{
    or(&cpu->regs.A, operand);
}

void rst_30()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0030;
}

void ldhl_sp_x(uchar operand)
{
    ushort result = cpu->regs.SP + (signed char)operand;
    
    if(result & 0x10000) gb_set_sf(kGBSFCarry);
    else gb_unset_sf(kGBSFCarry);
    
    if(((cpu->regs.SP & 0x0f) + (operand & 0x0f)) > 0x0f) gb_set_sf(kGBSFHalfCarry);
    else gb_unset_sf(kGBSFHalfCarry);
    
    gb_unset_sf(kGBSFZero);
    gb_unset_sf(kGBSFSubtract);
    
    cpu->regs.HL = result & 0xffff;
}

void ld_sp_hl()
{
    cpu->regs.SP = cpu->regs.HL;
}

void ld_a_xxp(ushort operand)
{
    uchar byte = readByte(operand);
    cpu->regs.A = byte;
}

void ei()
{
    gb_interrupt_set_master(1);
}

void cp_x(uchar operand)
{
    cp(operand);
}

void rst_38()
{
    pushShortToStack(cpu->regs.PC);
    cpu->regs.PC = 0x0038;
}
