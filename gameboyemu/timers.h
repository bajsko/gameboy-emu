//
//  timers.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-10.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef timers_h
#define timers_h

#include <stdio.h>
#include "cpu.h"

static const uchar kGBTimerClockSpeed4194;
static const uchar kGBTimerClockSpeed268400;
static const uchar kGBTimerClockSpeed67110;
static const uchar kGBTimerClockSpeed16780;

static const ushort kGBTimerDivReg = 0x4;
static const ushort kGBTimerCounterReg = 0x5;
static const ushort kGBTimerModuloReg = 0x6;
static const ushort kGBTimerControlReg = 0x7;

typedef struct gb_timers
{
    uchar div, counter, modulo, control;
    int divCounter, timerCounter;
} gb_timers_t;

void gb_timers_attach(cpu_t* cpu);
void gb_timers_step();
void gb_timers_detach();

gb_timers_t* gb_timers_get_struct();

void gb_timers_set_clock_freq();
int gb_timers_get_clock_freq();

#endif /* timers_h */
