//
//  config.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include "config.h"

static const input_mapping_t basic =
{
    GLFW_KEY_D,
    GLFW_KEY_A,
    GLFW_KEY_W,
    GLFW_KEY_S,
    GLFW_KEY_X,
    GLFW_KEY_C,
    GLFW_KEY_SPACE,
    GLFW_KEY_ENTER
};

input_mapping_t gb_config_get_basic_mapping()
{
    return basic;
}