//
//  input.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef input_h
#define input_h

#include <stdio.h>
#include "gbutil.h"

uchar gb_input_get_state();
void gb_input_key_pressed(uchar key);
void gb_input_key_released(uchar key);

#endif /* input_h */
