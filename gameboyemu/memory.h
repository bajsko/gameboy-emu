//
//  memory.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-12.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef memory_h
#define memory_h

#include <memory.h>

#include "cpu.h"

int gb_memory_attach(cpu_t* cpu);
int gb_memory_detach();

uchar readByte(ushort offset);
uint16_t readShort(ushort offset);

void writeByte(ushort offset, uchar byte);
void writeShort(ushort offset, ushort shortint);

ushort popShortFromStack();
void pushShortToStack(ushort value);

uchar getRawJoypad();

#endif /* memory_h */
