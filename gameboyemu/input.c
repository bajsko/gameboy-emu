//
//  input.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include "input.h"
#include "memory.h"
#include "interrupts.h"
#include <assert.h>

static uchar joypadState = 0xFF;

uchar gb_input_get_state()
{
    return joypadState;
}

void gb_input_key_pressed(uchar key)
{
    
    uchar joyinfo = getRawJoypad();
    uchar reqInt = 0;
    
    uchar previouslyUnset = 0;
    if(fetchBit(&joypadState, key) == 0)
        previouslyUnset = 1;
    
    //set key down
    setBit(&joypadState, key, 0);
    
    uchar button = 0;
    if(key > 3)
        button = 1;
    else
        button = 0;
    
    if(button && (fetchBit(&joyinfo, 5) == 1))
        reqInt = 1;
    else if(!button && (fetchBit(&joyinfo, 4) == 1))
        reqInt = 1;
    
    if(reqInt && previouslyUnset == 0) {
        gb_interrupt_request(kGBIFJoypad);
    }
    
}

void gb_input_key_released(uchar key)
{
    setBit(&joypadState, key, 1);
}
