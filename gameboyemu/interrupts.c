//
//  interrupts.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-21.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <assert.h>

#include "interrupts.h"
#include "gbutil.h"
#include "memory.h"

static cpu_t* cpu = NULL;

static uchar IE = 0;
static uchar IF = 0;
static uchar master = 0;

void gb_interrupt_attach(cpu_t* lcpu)
{
    assert(lcpu != NULL && cpu == NULL);
    cpu = lcpu;
}

void gb_interrupt_set_ie(uchar value)
{
    IE = value;
}

void gb_interrupt_set_if(uchar value)
{
    IF = value;
}

void gb_interrupt_set_master(uchar value)
{
    master = value;
}

uchar gb_interrupt_read_ie(uchar flag)
{
    assert(cpu != NULL);
    
    return (IE & (1 << flag)) >> flag;
}

uchar gb_interrupt_read_if(uchar flag)
{
    assert(cpu != NULL);
    
    return (IF & (1 << flag)) >> flag;
}

uchar gb_interrupt_get_ie()
{
    return IE;
}

uchar gb_interrupt_get_if()
{
    return IF;
}

uchar gb_interrupt_get_master()
{
    return master;
}

static void SetIFFlag(uchar flag, uchar mode)
{
    setBit(&IF, flag, mode);
}

static void ProcessInterrupt(uchar index)
{
    master = 0;
    SetIFFlag(index, 0);
    
    pushShortToStack(cpu->regs.PC);
    ushort pc = 0;
    
    switch (index) {
        case kGBIFVBlank:
            pc = 0x40;
            //vblanks++;
            break;
            
        case kGBIFLCDStat:
            pc = 0x48;
            break;
            
        case kGBIFTimer:
            pc = 0x50;
            break;
            
        case kGBIFJoypad:
            pc = 0x60;
            break;
            
        default:
            gb_log("FAILED PROCESSING INTERRUPT");
            pc = 0x0;
            break;
    }
    
    //unset halt flag
    gb_unset_sf(kGBSFHalt);
    cpu->regs.PC = pc;
    
}

void gb_interrupt_step()
{
    if(master == 0)
        return;
    
    if(IF > 0)
    {
        for(int i = 0; i < 5; i++)
        {
            if(gb_interrupt_read_if(i) == 1)
            {
                if(gb_interrupt_read_ie(i) == 1)
                {
                    ProcessInterrupt(i);
                }
            }
        }
    }
    
}

void gb_interrupt_request(uchar interrupt)
{
    SetIFFlag(interrupt, 1);
}

void gb_interrupt_detach()
{
    assert(cpu != NULL);
    cpu = NULL;
    
    gb_log("Interrupt: Detached from CPU!");
}
