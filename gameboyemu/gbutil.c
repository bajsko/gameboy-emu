//
//  gbutil.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-11.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "gbutil.h"

static unsigned long start_clock = 0;

uchar fetchBit(uchar* src, int bit)
{
    assert(src != NULL);
    
    uchar cpy = *src;
    cpy = (cpy >> bit) & 0x1;
    
    return cpy;
}

void setBit(uchar* src, uchar bit, uchar value)
{
    assert(src != NULL && bit <= 7);
    
    if(value > 0)
    {
        *src |= (1 << bit);
    } else
    {
        uchar srccpy = *src;
        srccpy = ~srccpy;
        srccpy |= (1 << bit);
        srccpy = ~srccpy;
        *src = srccpy;
    }
    
}

void gb_log(const char* format, ...)
{
    printf("GB Log: ");
    
    va_list ap;
    va_start(ap, format);
    vprintf(format, ap);
    va_end(ap);
    
    printf("\n");
}

void gb_stopwatch_start()
{
    start_clock = 0;
    clock_t clocks = clock();
    start_clock = clocks;
}

float gb_stopwatch_end()
{
    clock_t clocks = clock();
    return (float)((clocks - start_clock) / CLOCKS_PER_SEC);
}
