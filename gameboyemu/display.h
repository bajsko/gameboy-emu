//
//  display.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-20.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef display_h
#define display_h

#include <stdio.h>
#include "shader.h"
#include "mesh.h"
#include "gbutil.h"

typedef struct display
{
    shader_t* shader;
    mesh_t* quad;
    unsigned char framebuffer[GB_RES_Y][GB_RES_X][3]; //x, y, rgb
} display_t;

display_t* display_make();
void display_update(display_t* display);
void display_delete(display_t* display);

#endif /* display_h */
