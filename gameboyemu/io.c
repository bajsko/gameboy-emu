//
//  io.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-08.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <memory.h>
#include <assert.h>

#include "io.h"
#include "gbutil.h"

static int error = 0;

static void reportError(int errorCode) { error = errorCode; }

int readFileToBuffer(unsigned char* dst, const char* string, unsigned long* fileSize)
{
    assert(dst != NULL || string != NULL || fileSize != NULL);
    
    FILE* file = fopen(string, "rb");
    if(!file)
    {
        reportError(GB_IO_ERROR_OPEN);
        return -1;
    }
    
    fseek(file, 0, SEEK_END);
    long fsize = ftell(file);
    fseek(file, 0, SEEK_SET);
    
    unsigned long s = fread(dst, sizeof(unsigned char), fsize, file);
    if(s != fsize) {
        reportError(GB_IO_ERROR_READ);
        fclose(file);
        return -1;
    }
    
    fclose(file);
    
    if(fileSize)
        *fileSize = fsize;
    
    return 0;
}

int gb_getIOError()
{
    return error;
}