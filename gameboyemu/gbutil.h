//
//  gbutil.h
//  gameboyemu
//
//  Small utility functions to aid development
//
//  Created by Klas Henriksson on 2016-09-11.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef gbutil_h
#define gbutil_h

#define GB_IO_ERROR_OPEN -1
#define GB_IO_ERROR_READ -2
#define GB_ERROR_MEMORY -3
#define GB_ERROR_CPU_ALREADY_CREATED -4
#define GB_ERROR_NO_CPU -5

#define GB_RES_X 160
#define GB_RES_Y 144

#define GB_SCREEN_X 512
#define GB_SCREEN_Y 512

#include <stdio.h>

typedef unsigned char uchar;
typedef unsigned short ushort;

uchar fetchBit(uchar* src, int bit);
void setBit(uchar* src, uchar bit, uchar value);
void gb_log(const char* format, ...);
void gb_stopwatch_start();
float gb_stopwatch_end();

#endif /* gbutil_h */
