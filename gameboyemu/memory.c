//
//  memory.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-12.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <assert.h>

#include "memory.h"
#include "interrupts.h"
#include "gbutil.h"
#include "gpu.h"
#include "input.h"
#include "timers.h"

static cpu_t* cpu = NULL;

static uchar GetJoyadState()
{
    uchar res = getRawJoypad();
    res ^= 0xff;
    
    uchar keys = gb_input_get_state();
    
    if(fetchBit(&res, 4) == 0) //we are interessted in the direction keys, lower nibble
    {
        uchar nibble = keys >> 4;
        nibble |= 0xF0;
        res &= nibble;
    } else if(fetchBit(&res, 5) == 0)
    {
        uchar nibble = keys & 0xF;
        nibble |= 0xF0;
        res &= nibble;
    }
    
    return res;
}

//correct
static void DMATransfer(uchar data)
{
    ushort adress = data << 8;
    for(uchar i = 0; i < 0xA0; i++)
    {
        writeByte(0xfe00+i, readByte(adress+i));
    }
}

int gb_memory_attach(cpu_t* lcpu)
{
    assert(lcpu != NULL && cpu == NULL);
    cpu = lcpu;
    
    gb_log("Memory: Attached to CPU!");
    
    return 0;
}

int gb_memory_detach()
{
    assert(cpu != NULL);
    cpu = NULL;
    
    gb_log("Memory: Detached from CPU!");
    
    return 0;
}


uchar readByte(ushort offset)
{
    assert(cpu != NULL);
    ushort c = 0;
    
    if(offset <= 0x7fff)
    {
        c = cpu->rom[offset];
        return c;
    }
    
    else if(offset >= 0x8000 && offset <= 0x9fff)
    {
        offset -= 0x8000;
        c = cpu->vram[offset];
        return c;
    }
    
    else if(offset >= 0xa000 && offset <= 0xbfff)
    {
        offset -= 0xa000;
        c = cpu->sram[offset];
        return c;
    }
    
    else if(offset >= 0xc000 && offset <= 0xdfff)
    {
        offset -= 0xc000;
        c = cpu->ram[offset];
        return c;
    }
    
    else if(offset >= 0xe000 && offset <= 0xfdff)
    {
        offset -= 0xe000;
        c = cpu->ram[offset];
        return c;
    }
    
    else if(offset >= 0xfe00 && offset <= 0xfeff)
    {
        offset -= 0xfe00;
        c = cpu->oam[offset];
        return c;
    }
    
    else if(offset == 0xff40)
    {
        return gb_gpu_get_data()->lcdc;
    }
    
    else if(offset == 0xff41)
    {
        return gb_gpu_get_data()->stat;
    }
    
    else if(offset == 0xff42)
    {
        return gb_gpu_get_data()->scroll_y;
    }
    
    else if(offset == 0xff43)
    {
        return gb_gpu_get_data()->scroll_x;
    }
    
    else if(offset == 0xff44)
    {
        return gb_gpu_get_data()->scanline;
    }
    
    else if(offset == 0xff45)
    {
        return gb_gpu_get_data()->scanlineCompare;
    }
    
    else if(offset == 0xff4a)
    {
        return gb_gpu_get_data()->window_y;
    }
    
    else if(offset == 0xff4b)
    {
        return gb_gpu_get_data()->window_x;
    }
    
    else if(offset == 0xff04)
    {
        return gb_timers_get_struct()->div;
    }
    
    else if(offset == 0xff05)
    {
        return gb_timers_get_struct()->counter;
    }
    
    else if(offset == 0xff06)
    {
        return gb_timers_get_struct()->modulo;
    }
    
    else if(offset == 0xff07)
    {
        return gb_timers_get_struct()->control;
    }
    
    else if(offset == 0xff0f)
    {
        return gb_interrupt_get_if();
    }
    
    else if(offset >= 0xff80 && offset <= 0xfffe)
    {
        offset -= 0xff80;
        c = cpu->hram[offset];
        return c;
    }
    
    else if(offset >= 0xff00 && offset <= 0xff7f)
    {
        if(offset == 0xff00)
        {
            return GetJoyadState();
        }
        
        offset -= 0xff00;
        c = cpu->io[offset];
        return c;
    }
    
    else if(offset == 0xffff)
    {
        return gb_interrupt_get_ie();
    }
    
    return c;
}

//the z80 processor (or the GameBoy system) uses little endian
uint16_t readShort(ushort offset)
{
    
    uchar buff[2] = { 0 };
    buff[0] = readByte(offset);
    buff[1] = readByte(offset + 1);
    
    uint16_t res = 0;
    res |= (buff[1] << 8);
    res |= buff[0] & 0xFF;
    
    return res;
}

void writeByte(ushort offset, uchar byte)
{
    assert(cpu != NULL);
    
    if(offset >= 0x8000 && offset <= 0x9fff)
    {
        offset -= 0x8000;
        cpu->vram[offset] = byte;
    }
    
    else if(offset >= 0xa000 && offset <= 0xbfff)
    {
        offset -= 0xa000;
        cpu->sram[offset] = byte;
    }
    
    else if(offset >= 0xc000 && offset <= 0xdfff)
    {
        offset -= 0xc000;
        cpu->ram[offset] = byte;
    }
    
    else if(offset >= 0xe000 && offset <= 0xfdff)
    {
        offset -= 0xe000;
        cpu->ram[offset] = byte;
    }
    
    else if(offset >= 0xfe00 && offset <= 0xfeff)
    {
        offset -= 0xfe00;
        cpu->oam[offset] = byte;
    }
    
    else if(offset == 0xff40)
    {
        gb_gpu_get_data()->lcdc = byte;
    }
    
    else if(offset == 0xff41)
    {
        gb_gpu_get_data()->stat = byte;
    }
    
    else if(offset == 0xff42)
    {
        gb_gpu_get_data()->scroll_y = byte;
    }
    
    else if(offset == 0xff43)
    {
        gb_gpu_get_data()->scroll_x = byte;
    }
    
    else if(offset == 0xff45)
    {
        gb_gpu_get_data()->scanlineCompare = byte;
    }
    
    else if(offset == 0xff46)
    {
        DMATransfer(byte);
    }
    
    else if(offset == 0xff4a)
    {
        gb_gpu_get_data()->window_y = byte;
    }
    
    else if(offset == 0xff4b)
    {
        gb_gpu_get_data()->window_x = byte;
    }
    
    else if(offset == 0xff04)
    {
        gb_timers_get_struct()->div = 0;
    }
    
    else if(offset == 0xff05)
    {
        gb_timers_get_struct()->counter = byte;
    }
    
    else if(offset == 0xff06)
    {
        gb_timers_get_struct()->modulo = byte;
    }
    
    else if(offset == 0xff07)
    {
        int currFreq = gb_timers_get_clock_freq();
        gb_timers_get_struct()->control = byte;
        int newFreq = gb_timers_get_clock_freq();
        
        if(currFreq != newFreq)
            gb_timers_set_clock_freq();
    }
    
    else if(offset == 0xff0f)
    {
        gb_interrupt_set_if(byte);
    }
    
    else if(offset >= 0xff80 && offset <= 0xfffe)
    {
        offset -= 0xff80;
        cpu->hram[offset] = byte;
    }
    
    else if(offset >= 0xff00 && offset <= 0xff7f)
    {
        offset -= 0xff00;
        cpu->io[offset] = byte;
    }
    
    else if(offset == 0xffff)
    {
        gb_interrupt_set_ie(byte);
    }
}

void writeShort(ushort offset, ushort shortint)
{
    
    uchar b1 = shortint & 0x00FF;
    uchar b2 = (shortint & 0xFF00) >> 8;
    
    writeByte(offset, (uchar)b1);
    writeByte(offset + 1, (uchar)b2);
}

ushort popShortFromStack()
{
    ushort ret = readShort(cpu->regs.SP);
    cpu->regs.SP += 2;
    
    //gb_log("STACK: popped value 0x%02X", ret);
    
    return ret;
}

void pushShortToStack(ushort value)
{
    cpu->regs.SP-=2;
    writeShort(cpu->regs.SP, value);
    
    //gb_log("STACK: pushed value 0x%02X", value);
}

uchar getRawJoypad()
{
    return cpu->io[0];
}
