//
//  config.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef config_h
#define config_h

#include <stdio.h>
#include <GLFW/glfw3.h>

#include "gbutil.h"

typedef struct input_mapping
{
    int right;
    int left;
    int up;
    int down;
    int a;
    int b;
    int select;
    int start;
} input_mapping_t;

input_mapping_t gb_config_get_basic_mapping();

#endif /* config_h */
