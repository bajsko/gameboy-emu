//
//  interrupts.h
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-21.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef interrupts_h
#define interrupts_h

#include <stdio.h>

#include "cpu.h"

static const uchar kGBIFVBlank = 0;
static const uchar kGBIFLCDStat = 1;
static const uchar kGBIFTimer = 2;
static const uchar kGBIFSerial = 3;
static const uchar kGBIFJoypad = 4;

void gb_interrupt_attach(cpu_t* cpu);

void gb_interrupt_set_ie(uchar value);
void gb_interrupt_set_if(uchar value);
void gb_interrupt_set_master(uchar value);

uchar gb_interrupt_read_ie(uchar flag);
uchar gb_interrupt_read_if(uchar flag);

uchar gb_interrupt_get_ie();
uchar gb_interrupt_get_if();
uchar gb_interrupt_get_master();

void gb_interrupt_step();

void gb_interrupt_request(uchar interrupt);

void gb_interrupt_detach();

uchar* get_vblank_pointer();
#endif /* interrupts_h */
