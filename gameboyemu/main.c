//
//  main.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-09-07.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <GLFW/glfw3.h>
#include <stdio.h>

#include "cpu.h"
#include "gpu.h"
#include "interrupts.h"
#include "gbutil.h"
#include "display.h"
#include "input.h"
#include "config.h"
#include "timers.h"

input_mapping_t mapping;
void key_callback(GLFWwindow* window, int key, int scandcode, int action, int mods);

int main(int argc, const char * argv[]) {
    
    if(!glfwInit())
    {
        return -1;
    }
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    GLFWwindow* window = glfwCreateWindow(GB_SCREEN_X, GB_SCREEN_Y, "GameBoy Emulator", NULL, NULL);
    if(!window)
    {
        glfwTerminate();
        gb_log("Could not create window!");
        return -1;
    }
    
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    
    glViewport(0, 0, GB_SCREEN_X, GB_SCREEN_Y);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    display_t* display = display_make();
    if(!display)
    {
        glfwDestroyWindow(window);
        glfwTerminate();
        return 0;
    }
    
    glfwSetKeyCallback(window, key_callback);
    mapping = gb_config_get_basic_mapping();
    
    int ok = gb_cpu_init(&ok);
    if(ok != 0)
        return -1;
    
    gb_gpu_set_frame_buffer(display->framebuffer);
    ok = gb_load_cartridge("tetris.gb");
    if(ok != 0)
        return -1;
    
    int cycles_per_second = 4194304;
    int fps = 60;
    int max_cpu_cycles = (int)(cycles_per_second/fps);
    int cycles = 0;
    //unsigned long time = 0;
    
    double lastTime = glfwGetTime();
    double nbFrames = 0;
    
    while(!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        
        //fps
        if((glfwGetTime() - lastTime) >= 1)
        {
            lastTime += 1;
            printf("FPS: %f\n", nbFrames);
            nbFrames = 0;
        }
        nbFrames++;
        
        while(cycles < max_cpu_cycles)
        {
            gb_cpu_step();
            gb_timers_step();
            gb_gpu_step();
            gb_interrupt_step();
            cycles += gb_get_cycles();
        }
        
        
        cycles = 0;
        
        display_update(display);
        glfwSwapBuffers(window);
        
        
    }
    
    gb_cpu_free();
    
    display_delete(display);
    
    glfwDestroyWindow(window);
    glfwTerminate();
    
    return 0;
}

void key_callback(GLFWwindow* window, int key, int scandcode, int action, int mods)
{
    
    /*if(action == GLFW_REPEAT)
        return;*/
    
    uchar gbkey = 0;
    
    if(key == mapping.right)
        gbkey = 0;
    else if(key == mapping.left)
        gbkey = 1;
    else if(key == mapping.up)
        gbkey = 2;
    else if(key == mapping.down)
        gbkey = 3;
    else if(key == mapping.a)
        gbkey = 4;
    else if(key == mapping.b)
        gbkey = 5;
    else if(key == mapping.select)
        gbkey = 6;
    else if(key == mapping.start)
        gbkey = 7;
    else if(key == GLFW_KEY_T && action == GLFW_PRESS) {
        gb_toggle_debug();
        return;
    }
    else
        return;
    
    if(action == GLFW_PRESS || action == GLFW_REPEAT) {
        gb_input_key_pressed(gbkey);
    }
    else if(action == GLFW_RELEASE)
        gb_input_key_released(gbkey);
    
}
