//
//  timers.c
//  gameboyemu
//
//  Created by Klas Henriksson on 2016-10-10.
//  Copyright © 2016 bajsko. All rights reserved.
//

#include <assert.h>

#include "timers.h"
#include "memory.h"
#include "interrupts.h"

static gb_timers_t timers = { 0, 0, 0, 0, 0, 0 };
static cpu_t* cpu = NULL;

void gb_timers_attach(cpu_t* lcpu)
{
    assert(lcpu != NULL && cpu == NULL);
    
    cpu = lcpu;
    
    gb_log("Timers: Attached to CPU!");
}

static void UpdateDIV(int cycles)
{
    timers.divCounter += cycles;
    if(timers.divCounter >= 255)
    {
        timers.divCounter = 0;
        timers.div++;
    }
}

gb_timers_t* gb_timers_get_struct()
{
    return &timers;
}

void gb_timers_set_clock_freq()
{
    uchar freq = timers.control & 0x3;
    int counter = 0;
    switch (freq) {
        case 0:
            counter = 1024;
            break;
            
        case 1:
            counter = 16;
            break;
            
        case 2:
            counter = 64;
            break;
            
        case 3:
            counter = 256;
            break;
            
        default:
            assert(NULL);
            break;
    }
    
    timers.timerCounter = counter;
}

int gb_timers_get_clock_freq()
{
    return timers.timerCounter;
}

void gb_timers_step()
{
    int cycles = (int)cpu->cycles;
    UpdateDIV(cycles);
    
    if(fetchBit(&timers.control, 2) == 0)
        return;
    
    timers.timerCounter -= cycles;
    if(timers.timerCounter <= 0)
    {
        gb_timers_set_clock_freq();
        
        if(timers.counter == 255)
        {
            timers.counter = timers.modulo;
            gb_interrupt_request(kGBIFTimer);
        } else
        {
            timers.counter++;
        }
    }
}

void gb_timers_detach()
{
    cpu = NULL;
    gb_log("Timers: Detached from CPU!");
}