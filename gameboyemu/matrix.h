//
//  matrix.h
//  gameboyemu
//
//  Code from my old multisnake project.
//
//  Created by Klas Henriksson on 2016-09-19.
//  Copyright © 2016 bajsko. All rights reserved.
//

#ifndef matrix_h
#define matrix_h

#include <stdio.h>
#include <math.h>
#include <string.h>

//set matrix to an identity matrix
void mat_identity(float* matrix);
//set matrix to a translation matrix with given coordinates
void mat_translate(float* matrix, float dx, float dy, float dz);
//set matrix to a scaling matrix with given scalar
void mat_scale(float* matrix, float scalar);
//set matrix to a rotation matrix with given values (angle in radians)
void mat_rotate(float* matrix, float x, float y, float z, float angle);

//setup an orthographic projection
void mat_ortho(float* matrix, float left, float right, float top, float bottom, float near, float far);
//setup an orthographic projectiion mapped to given width & height
void mat_set_mat2d(float* matrix, int width, int height);


#endif /* matrix_h */
